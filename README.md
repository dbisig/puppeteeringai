# Puppeteering AI

Daniel Bisig - Coventry University, UK - [ad5041@coventry.ac.uk](ad5041@coventry.ac.uk)

## Abstract

Puppeteering AI is a system that creates an artificial dancer whose movements are generated through a combination of interactive control and machine learning. Puppeteering AI explores interactive applications of an autoencoder that has been trained on motion capture recordings of a human dancer. These explorations have led to the discovery, that autoencoders, when applied iteratively on their own decoded output, can produce interesting synthetic motions, in particular when some of the values of this output are removed from the autoencoder’s influence and instead controlled interactively. This approach is reminiscent of puppeteering, in which a puppeteer only controls a subset of a puppet’s joints while the other joints are taken care of by another process. In traditional puppeteering, this other process is physics, whereas in the system presented here its is autoencoding. While physics ensures that the puppet performs physically plausible motions, autoencoding ensures that the artificial dancer exhibits motions that are stylistically plausible to the extent that they resemble movements that have been recorded from a dancer.

This project has been realised in collaboration with Ephraim Wegner, teacher and researcher at Offenburg University, Germany. A detailed description of the project has been [published](https://www.researchgate.net/profile/Daniel-Bisig/publication/356788649_Puppeteering_an_AI_-_Interactive_Control_of_a_Machine-Learning_based_Artificial_Dancer/links/61ad1bf550e22929cd4dff4f/Puppeteering-an-AI-Interactive-Control-of-a-Machine-Learning-based-Artificial-Dancer.pdf?_sg[0]=puj8cm-sWyP1s0gJZlomMVnfxwLIS0Q4cElXrPaBSd2oJPmrRQSytcmtuvplQd_AOAThHBhtcTIz5Ecslh4tFw.MiGn-rPQ0z1Bcf6CUHdhtmZV5ngX70loc8xR52m9sYVx1bl4geZ4LMwa8p2I1aWSkNyjseEbY10_5VeD4Yk38w.R3ZvW9N6B95hGd3DXWvDFYAoIxruMt59fZTrNJyQHeHzrNlvct8kYijHzIyzKnm9n0cv37bxGpZ1KZNmEAUCGQ&_sg[1]=sAz3eVxAk0UK7BSz_J4UcBlXPeosVULaxtbYBuP322CD_4SMGL3pmFYlr4BUg9iUEE7WDfDklsUOEsjMiAvlfbxeWIqyCWQ_wkfmJx_BiPlm.MiGn-rPQ0z1Bcf6CUHdhtmZV5ngX70loc8xR52m9sYVx1bl4geZ4LMwa8p2I1aWSkNyjseEbY10_5VeD4Yk38w.R3ZvW9N6B95hGd3DXWvDFYAoIxruMt59fZTrNJyQHeHzrNlvct8kYijHzIyzKnm9n0cv37bxGpZ1KZNmEAUCGQ&_iepl=).

## Resources

- E2-Create [Project Page](https://wp.coventry.domains/e2create/ "Project Page")