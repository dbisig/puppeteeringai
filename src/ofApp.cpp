#include "ofApp.h"
#include <torch/script.h>
#include <torch/nn.h>
#include <memory>
#include <iostream>
#include <cmath>
#include <typeinfo>
#include <type_traits>
#include "dab_exception.h"
#include "dab_ringbuffer.h"
#include "dab_vis_skeleton.h"
#include "dab_torch_quat.h"
#include "dab_visualization.h"
#include "dab_torch_quat.h"

using namespace torch::indexing;

//--------------------------------------------------------------
void ofApp::setup()
{
	try
	{
		setupMocap();
		loadMocapFile("../data/example_mocap.json");

		setupOsc();

		setupML("../models/model8/disc_prior_cpu.pt",
			"../models/model8/encoder_cpu.pt",
			"../models/model8/decoder_cpu.pt");
		updateML();

		setupGraphics();

		mGui.setup();
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << "\n";
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}
}

//--------------------------------------------------------------
void ofApp::update()
{	
	try
	{
		updateOsc();
		updateMocap();
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button)
{
	if (button == 2)
	{
		if (mMouseDragActive == false)
		{
			mMouseDragActive = true;
			mMouseDragStartPos = glm::vec2(x, y);
		}
		else
		{
			glm::vec2 mouseDragVec = glm::vec2(x, y) - mMouseDragStartPos;
			mCameraAzumith += mouseDragVec.y / static_cast<float>(ofGetWindowHeight()) * M_PI * 0.1;
			mCameraElevation += mouseDragVec.x / static_cast<float>(ofGetWindowWidth()) * M_PI * 0.1;

			//std::cout << "drag x " << mouseDragVec.x << " y " << mouseDragVec.y << " az " << mCameraAzumith << " el " << mCameraElevation << "\n";

			updateCamera();
		}
	}
	else
	{
		mMouseDragActive = false;
	}
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

void ofApp::mouseScrolled(int x, int y, float scrollX, float scrollY)
{
	//std::cout << "scrollX " << scrollX << " scrollY " << scrollY << "\n";

	mCameraDistance += scrollY * 10.0;
	updateCamera();
}


//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

//--------------------------------------------------------------

void 
ofApp::setupMocap()
{
	mMocapWindowLength = 8;
	mMocapWindowOffset = 1;
	mMocapJointCount = 29; // will be overwritten in the loadMocapFile call
	mMocapJointDim = 4;
	mMocapLatentDim = 16;

	mMocapLiveRingBuffer = new dab::RingBuffer< std::array<float, 4> >(std::array<float, 4>({ 1.0, 0.0, 0.0, 0.0 } ), mMocapWindowLength + 1);
	mMocapLiveValues = std::vector<float>(mMocapWindowLength * mMocapJointDim);
	for (int tI = 0, vI=0; tI < mMocapWindowLength; ++tI)
	{
		for (int dI = 0; dI < 4; ++dI, ++vI)
		{
			mMocapLiveValues[vI] = (*mMocapLiveRingBuffer)[tI][dI];
		}
	}
	mMocapLiveJointMap = LeftArm;
}

void 
ofApp::loadMocapFile(const std::string& pFilePath) throw (dab::Exception)
{
	try
	{
		mMocapDataset.load(pFilePath);

		mPoseSequence = mMocapDataset.subjectData("S1").data("rot_local").values();
		mMocapJointCount = mMocapDataset.subjectData("S1").skeleton().jointCount();
		mMocapReadFrame = mMocapWindowOffset;

		// add trajectory data
		std::vector< std::string >& subjectNames = mMocapDataset.subjectNames();

		for (const std::string& subjectName : subjectNames)
		{
			dab::MocapSubjectData& subjectData = mMocapDataset.subjectData(subjectName);
			const dab::MocapData& posWorldData = subjectData.data("pos_world");
			unsigned int sequenceLength = posWorldData.dim()[0];
			unsigned int jointCount = posWorldData.dim()[1];

			// find root joint index
			const std::vector< int >& jointParents = subjectData.skeleton().jointParents();
			int64_t rootJointIndex = std::find(jointParents.begin(), jointParents.end(), -1) - jointParents.begin();

			// create trajectory data
			const torch::Tensor& posWorldTensor = posWorldData.values();
			torch::Tensor rootTrajectoryTensor = posWorldTensor.index({ Slice(None), rootJointIndex, Slice(None) }).clone().detach();

			dab::MocapData rootTrajectoryData("trajectory", rootTrajectoryTensor);
			subjectData.addData(rootTrajectoryData);
		}

		mPoseSequence = mMocapDataset.subjectData("S1").data("rot_local").values();
		mBasePose = mPoseSequence[0];
		mMocapFrameCount = mPoseSequence.sizes()[0];

		mMocapStartFrame = mMocapReadFrame;
		mMocapEndFrame = mMocapFrameCount;
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("MOCAP ERROR: failed to load mocap data", __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void
ofApp::updateMocap()
{
	//std::cout << "ofApp::updateMocap()\n";

	int playFrame = mVisSkeletonPlayer->playFrame();

	//std::cout << "playFrame " << playFrame << "\n";

	if (playFrame == mMocapWindowOffset - 1) // player reached end of playable sequence
	{
		mVisSkeletonPlayer->setPositionSequence(mPositionSequenceUpdateVector);

		if (mMLThreadDone == true)
		{
			mMLThreadHandle = std::async(std::launch::async, &ofApp::updateML, this);
		}
	}
}

void
ofApp::updateMocap(const std::vector<float>& pMocapSample) throw (dab::Exception)
{
	std::array<float, 4> imuOrient = { pMocapSample[6], pMocapSample[7], pMocapSample[8], pMocapSample[9] };

	mMocapLiveRingBuffer->update(imuOrient);

	for (int sI = 0, vI = 0; sI < mMocapWindowLength; ++sI)
	{
		const std::array<float, 4>& mocapSample = (*mMocapLiveRingBuffer)[mMocapWindowLength - (sI + 1)];

		for (int dI = 0; dI < 4; ++dI, ++vI)
		{
			mMocapLiveValues[vI] = mocapSample[dI];
		}
	}
}

void
ofApp::setupOsc()
{
	try
	{
		mSelf = std::shared_ptr<ofApp>(this);
		mOscReceiver = new dab::OscReceiver("MocapReceiver", 9002);
		mOscReceiver->registerOscListener(std::weak_ptr<ofApp>(mSelf));
		mOscReceiver->start();

		mOscSendAddress = "127.0.0.1";
		mOscSendPort = 9003;
		mOscSender = new dab::OscSender("MocapSender", mOscSendAddress, mOscSendPort);
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}
}

void
ofApp::notify(std::shared_ptr<dab::OscMessage> pMessage)
{
	mOscLock.lock();

	mOscMessageQueue.push_back(pMessage);
	if (mOscMessageQueue.size() > mMaxOscMessageQueueLength) mOscMessageQueue.pop_front();

	mOscLock.unlock();
}

void
ofApp::updateOsc()
{
	mOscLock.lock();

	while (mOscMessageQueue.size() > 0)
	{
		std::shared_ptr< dab::OscMessage > oscMessage = mOscMessageQueue[0];

		updateOsc(oscMessage);

		mOscMessageQueue.pop_front();
	}

	mOscLock.unlock();
}

void
ofApp::updateOsc(std::shared_ptr<dab::OscMessage> pMessage)
{
	try
	{
		std::string address = pMessage->address();

		//std::cout << "address " << address << "\n";

		const std::vector<dab::_OscArg*>& arguments = pMessage->arguments();

		if (address.compare("/imu/1") == 0) updateMocap(arguments);
		else if (address == "/vis/camera") updateCamera(arguments);
		else if (address == "/mocap/pos") setMocapReadFrame(arguments);
		else if (address == "/mocap/start") updateMocapStart(arguments);
		else if (address == "/mocap/end") updateMocapEnd(arguments);
		else if (address == "/ml/encodecount") updateEncodeIterCount(arguments);
		else if (address == "/ml/livecount") updateLiveIterCount(arguments);
		else if (address == "/mocap/livejoint") updateLiveJoint(arguments);
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}
}

void 
ofApp::updateOscSender() throw (dab::Exception)
{
	try
	{
		// send joint positions
		{
			const std::vector<glm::vec3>& currentPos = mPositionSequenceUpdateVector[0];
			int posCount = currentPos.size();

			std::string messageAddress = "/mocap/pos";
			std::shared_ptr<dab::OscMessage> message(new dab::OscMessage(messageAddress));

			for (int pI = 0; pI < posCount; ++pI)
			{
				message->add(currentPos[pI].x);
				message->add(currentPos[pI].y);
				message->add(currentPos[pI].z);
			}

			mOscSender->send(message);
		}

		// send joint rotations
		{
			const std::vector<glm::quat>& currentRot = mRotationSequenceUpdateVector[0];

			int rotCount = currentRot.size();

			std::string messageAddress = "/mocap/rot";
			std::shared_ptr<dab::OscMessage> message(new dab::OscMessage(messageAddress));

			for (int pI = 0; pI < rotCount; ++pI)
			{
				/*
				// send euler
				glm::vec3 euler = glm::eulerAngles(currentRot[pI]);

				message->add(euler[0]);
				message->add(euler[1]);
				message->add(euler[2]);
				*/

				// send quaternion
				message->add(currentRot[pI][0]);
				message->add(currentRot[pI][1]);
				message->add(currentRot[pI][2]);
				message->add(currentRot[pI][3]);
			}

			mOscSender->send(message);
		}
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}
}

void
ofApp::updateMocap(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception)
{
	try
	{
		int argCount = pArgs.size();
		std::vector<float> mocapSample(argCount);
		for (int aI = 0; aI < argCount; ++aI)
		{
			float value = *pArgs[aI];
			mocapSample[aI] = value;
		}

		//std::cout << "mocapSample " << mocapSample << "\n";

		updateMocap(mocapSample);
	}
	catch (dab::Exception& e)
	{
		throw e;
	}
}

void
ofApp::updateCamera(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception)
{
	try
	{
		mCameraAzumith = *pArgs[0];
		mCameraElevation = *pArgs[1];
		mCameraDistance = *pArgs[2];

		updateCamera();

		//std::cout << "mCameraAzumith " << mCameraAzumith << " mCameraElevation " << mCameraElevation << " mCameraDistance " << mCameraDistance << "\n";
	}
	catch (dab::Exception& e)
	{
		throw e;
	}
}

void 
ofApp::setMocapReadFrame(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception)
{
	try
	{
		int mocapReadFrame = *pArgs[0];
		mMocapReadFrame = std::min(std::max(mMocapStartFrame, mocapReadFrame), mMocapEndFrame);

		//std::cout << "mCameraAzumith " << mCameraAzumith << " mCameraElevation " << mCameraElevation << " mCameraDistance " << mCameraDistance << "\n";
	}
	catch (dab::Exception& e)
	{
		throw e;
	}
}

void 
ofApp::updateMocapStart(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception)
{
	try
	{
		int mocapStartFrame = *pArgs[0];
		mMocapStartFrame = std::min(std::max(0, mocapStartFrame), mMocapEndFrame);

		//std::cout << "mCameraAzumith " << mCameraAzumith << " mCameraElevation " << mCameraElevation << " mCameraDistance " << mCameraDistance << "\n";
	}
	catch (dab::Exception& e)
	{
		throw e;
	}
}

void 
ofApp::updateMocapEnd(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception)
{
	try
	{
		int mocapEndFrame = *pArgs[0];
		mMocapEndFrame = std::max(std::min(mMocapFrameCount, mocapEndFrame), mMocapStartFrame);

		//std::cout << "mCameraAzumith " << mCameraAzumith << " mCameraElevation " << mCameraElevation << " mCameraDistance " << mCameraDistance << "\n";
	}
	catch (dab::Exception& e)
	{
		throw e;
	}
}

void 
ofApp::updateEncodeIterCount(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception)
{
	try
	{
		int mocapEncodeIterCount = *pArgs[0];
		mMocapEncodeIterCount = std::min(10, std::max(mocapEncodeIterCount, 1));
	}
	catch (dab::Exception& e)
	{
		throw e;
	}
}

void 
ofApp::updateLiveIterCount(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception)
{
	try
	{
		int mocapLiveIterCount = *pArgs[0];
		mMocapLiveIterCount = std::min(mMocapEncodeIterCount, std::max(mocapLiveIterCount, 0));
	}
	catch (dab::Exception& e)
	{
		throw e;
	}
}

void 
ofApp::updateLiveJoint(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception)
{
	try
	{
		int liveJoint = *pArgs[0];

		mMocapLiveJointMap = static_cast<skeletonJointIndices>(liveJoint);
	}
	catch (dab::Exception& e)
	{
		throw e;
	}
}

void 
ofApp::setupGraphics() throw (dab::Exception)
{
	try
	{
		dab::Visualization& visualization = dab::Visualization::get();
		visualization.setBasePose(mPoseSequence[0]);

		ofDisableArbTex(); // important: otherwise all textures will be of type RECT2D instead of 2D
		ofDisableLighting();
		ofEnableDepthTest();
		ofSetVerticalSync(true);
		ofBackground(0);

		// create camera
		mCamera = std::shared_ptr<ofCamera>(new ofCamera());
		updateCamera();

		// create light source
		ofEnableLighting();
		ofSetSmoothLighting(true);
		mPointLight = std::make_shared<ofLight>();
		mPointLight->setPointLight();

		// create visual skeleton player
		std::string subjectName = mMocapDataset.subjectNames()[0];
		const dab::MocapSubjectData& subjectData = mMocapDataset.subjectData(subjectName);
		const dab::MocapSkeleton& subjectSkeleton = subjectData.skeleton();
		mVisSkeletonPlayer = std::make_shared<dab::VisSkeletonPlayer>(subjectSkeleton.jointChildren());

		// configure body materials
		std::shared_ptr<dab::VisSkeleton> visSkeleton = mVisSkeletonPlayer->skeleton();

		ofMaterial jointMaterial;
		ofMaterial edgeMaterial;

		std::array<float, 3> mSkeletonColor = {0.5, 0.5, 0.5};
		mAmbientLightIntensity = 0.2;
		mDiffuseLightIntensity = 0.5;
		mSpecularLightIntensity = 1.0;

		jointMaterial.setAmbientColor(ofColor::fromHsb(mSkeletonColor[0] * 255.0, mSkeletonColor[1] * 255.0, mSkeletonColor[2] * 255.0));
		jointMaterial.setDiffuseColor(ofColor::fromHsb(mSkeletonColor[0] * 255.0, mSkeletonColor[1] * 255.0, mSkeletonColor[2] * 255.0));
		jointMaterial.setSpecularColor(ofColor::fromHsb(mSkeletonColor[0] * 255.0, mSkeletonColor[1] * 255.0, mSkeletonColor[2] * 255.0));
		jointMaterial.setShininess(100.0);

		edgeMaterial.setAmbientColor(ofColor(mAmbientLightIntensity * 255.0, mAmbientLightIntensity * 255.0, mAmbientLightIntensity * 255.0));
		edgeMaterial.setDiffuseColor(ofColor(mDiffuseLightIntensity * 255.0, mDiffuseLightIntensity * 255.0, mDiffuseLightIntensity * 255.0));
		edgeMaterial.setSpecularColor(ofColor(mSpecularLightIntensity * 255.0, mSpecularLightIntensity * 255.0, mSpecularLightIntensity * 255.0));

		//edgeMaterial.setAmbientColor(ofColor::fromHsb(0.2 * 255.0, 0.2 * 255.0, 0.5 * 255.0));
		//edgeMaterial.setDiffuseColor(ofColor::fromHsb(0.2 * 255.0, 0.2 * 255.0, 0.5 * 255.0));
		//edgeMaterial.setSpecularColor(ofColor::fromHsb(0.2 * 255.0, 0.2 * 255.0, 0.5 * 255.0));
		edgeMaterial.setShininess(100.0);

		visSkeleton->setJointMaterial(jointMaterial);
		visSkeleton->setEdgeMaterial(edgeMaterial);

		// configure light settings
		mPointLight->setPosition(-100.0, 100.0, 21.935484);
		mPointLight->setAmbientColor(ofColor::fromHsb(0.0 * 255.0, 0.0 * 255.0, 0.13871 * 255.0));
		mPointLight->setAttenuation(0.745602, 0.0, 0.0);
		mPointLight->setDiffuseColor(ofColor::fromHsb(0.0 * 255.0, 0.0 * 255.0, 1.0 * 255.0));
		mPointLight->setSpecularColor(ofColor::fromHsb(0.0 * 255.0, 0.0 * 255.0, 1.0 * 255.0));

		// start playing right away
		mVisSkeletonPlayer->play(mPositionSequenceUpdateVector);
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("VIS ERROR: failed to setup graphics", __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void
ofApp::updateGraphics() throw (dab::Exception)
{
	// convert quaternion sequence into position sequence
	torch::Tensor sequenceExcerpt = mPoseSequenceUpdateBuffer.index({ Slice(None, mMocapWindowOffset) });
	sequenceExcerpt = sequenceExcerpt.unsqueeze(0);

	torch::Tensor zeroTrajectory = torch::zeros({ 1, mMocapWindowOffset, 3 }, torch::kFloat32);

	const dab::MocapSkeleton& mocapSkeleton = mMocapDataset.subjectData("S1").skeleton();
	torch::Tensor positionSequenceTensor = mocapSkeleton.forwardKinematics(sequenceExcerpt, zeroTrajectory);

	// convert position sequence from tensor to vector of glm::vec3
	dab::Visualization::get().tensor2glm(positionSequenceTensor[0], mPositionSequenceUpdateVector);
	dab::Visualization::get().tensor2glm(sequenceExcerpt[0], mRotationSequenceUpdateVector);
}

void 
ofApp::setupML(const std::string& pDiscPriorModelPath, const std::string& pEncoderModelPath, const std::string& pDecoderModelPath) throw (dab::Exception)
{
	try
	{
		// Deserialize the ScriptModule from a file using torch::jit::load().
		mDiscPrior = torch::jit::load(pDiscPriorModelPath);
		std::cout << "disc_prior loading ok\n";
		mEncoder = torch::jit::load(pEncoderModelPath);
		std::cout << "encoder loading ok\n";
		mDecoder = torch::jit::load(pDecoderModelPath);
		std::cout << "decoder loading ok\n";

		mPoseSequenceUpdateBuffer = torch::zeros({ mMocapWindowLength, mMocapJointCount, mMocapJointDim });

		mMocapEncodeIterCount = 1;
		mMocapLiveIterCount = 0;
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << "\n";
		std::cout << "error loading the model\n";

		throw dab::Exception("ML ERROR: failed to load models", __FILE__, __FUNCTION__, __LINE__);
	}
}

void
ofApp::updateML() throw (dab::Exception)
{
	if(mMocapReadFrame < mMocapStartFrame) mMocapReadFrame = mMocapStartFrame;

	torch::NoGradGuard no_grad;
	//std::cout << "updateML() begin\n";
	//double time1 = static_cast<double>(ofGetElapsedTimeMillis());

	mMLThreadDone = false;

	dab::TorchQuat& torchquat = dab::TorchQuat::get();
	torch::Tensor windowEnvelope = torch::hann_window(mMocapWindowLength, torch::kFloat32);

	// get mocap window from file
	//std::cout << "mMocapReadFrame " << mMocapReadFrame << "\n";
	torch::Tensor inputSequence = mPoseSequence.index({ Slice(mMocapReadFrame, mMocapReadFrame + mMocapWindowLength), Slice(None), Slice(None) });

	//// debug create randomized input sequence
	//torch::Tensor inputSequence = torch::randn({ mMocapWindowLength, mMocapJointCount, mMocapJointDim }, torch::kFloat32);
	//inputSequence = torch::nn::functional::normalize(inputSequence, torch::nn::functional::NormalizeFuncOptions().dim(2));

	// live joint orientation
	torch::Tensor liveJointInputSequence = torch::from_blob((float*)(mMocapLiveValues.data()), mMocapWindowLength * mMocapJointDim);
	liveJointInputSequence = liveJointInputSequence.view({ mMocapWindowLength, mMocapJointDim });

	torch::Tensor sequenceEncoded;
	torch::Tensor sequenceDecoded;

	for (int i = 0; i < mMocapEncodeIterCount; ++i)
	{
		// copy interactive joint data into input sequence
		if (i < mMocapLiveIterCount)
		{
			inputSequence.index({ Slice(None), mMocapLiveJointMap, Slice(None) }) = liveJointInputSequence;
		}

		// encode input sequence
		inputSequence = inputSequence.view({ 1, mMocapWindowLength, mMocapJointCount * mMocapJointDim });
		sequenceEncoded = mEncoder.forward({ inputSequence }).toTensor();

		//// random encoding vec
		//torch::Tensor sequenceEncoded = torch::randn({ 1, mMocapLatentDim }, torch::kFloat32) * 10.0;

		// get decoded sequence
		sequenceDecoded = mDecoder.forward({ sequenceEncoded }).toTensor();
		sequenceDecoded = sequenceDecoded.view({ mMocapWindowLength, mMocapJointCount, mMocapJointDim });

		inputSequence = sequenceDecoded;

		// copy interactive joint data into input sequence
		if (i < mMocapLiveIterCount)
		{
			inputSequence.index({ Slice(None), mMocapLiveJointMap, Slice(None) }) = liveJointInputSequence;
		}
	}

	// shift mPoseSequenceUpdateBuffer by windowOffset to left
	mPoseSequenceUpdateBuffer = torch::roll(mPoseSequenceUpdateBuffer, { -mMocapWindowOffset }, { 0 });

	// fill end of mPoseSequenceUpdateBuffer with default pose / values
	mPoseSequenceUpdateBuffer.index({ Slice(-mMocapWindowOffset, None) }) = mBasePose.reshape({ 1, mMocapJointCount, mMocapJointDim }).repeat({ mMocapWindowOffset, 1, 1 });

	// add decoded sequence to mPoseSequenceUpdateBuffer using quaternion slerp
	for (int sI = 0; sI < mMocapWindowLength; ++sI)
	{
		torch::Tensor currentQuat = mPoseSequenceUpdateBuffer.index({ sI, Slice(None) });
		torch::Tensor targetQuat = sequenceDecoded.index({ sI, Slice(None) });
		torch::Tensor quatMix = windowEnvelope[sI].repeat({ mMocapJointCount });
		torch::Tensor mixQuat = torchquat.slerp2(currentQuat, targetQuat, quatMix);

		mPoseSequenceUpdateBuffer.index({ sI, Slice(None) }) = mixQuat;
	}

	// debug: change live joint orientation on final output sequence
	// mPoseSequenceUpdateBuffer.index({ Slice(None), mMocapLiveJointMap }) = liveJointInputSequence;


	updateGraphics();
	updateOscSender();

	// TODO: only increment mMocapReadFrame when enough time has passed
	mMocapReadFrame += mMocapWindowOffset;

	//std::cout << "mMocapReadFrame " << mMocapReadFrame << "\n";

	if (mMocapReadFrame + mMocapWindowLength >= mMocapEndFrame)
	{
		mMocapReadFrame = mMocapStartFrame;
	}

	mMLThreadDone = true;

	//double time2 = static_cast<double>(ofGetElapsedTimeMillis());
	//std::cout << "updateML() duration " << (time2 - time1) / 1000.0 << "\n";
	//std::cout << "updateML() end\n";
}


//--------------------------------------------------------------
void ofApp::draw()
{
	ofBackground(255, 255, 255);

	try
	{
		displayGraphics();

		//mGui.begin();

		//ImGui::SliderInt("iterCount", &mMocapEncodeIterCount, 1, 10);
		//ImGui::SliderInt("liveIter", &mMocapLiveIterCount, 0, mMocapEncodeIterCount);

		//mGui.end();

		std::string frameString = "Frame: " + std::to_string(mMocapReadFrame) + " Start: " + std::to_string(mMocapStartFrame) + " End: " + std::to_string(mMocapEndFrame) + " Max: " + std::to_string(mMocapFrameCount);
		ofDrawBitmapString(frameString, 10.0, 15.0);
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}
}

void 
ofApp::displayGraphics() throw (dab::Exception)
{
	ofSetColor(255.0, 255.0, 255.0);

	ofEnableLighting();
	mPointLight->enable();

	mCamera->begin();
	mVisSkeletonPlayer->draw();
	mCamera->end();
}

void 
ofApp::updateCamera()
{
	glm::vec3 cameraPos;
	cameraPos.x = cos(mCameraAzumith) * sin(mCameraElevation) * mCameraDistance;
	cameraPos.y = sin(mCameraAzumith) * sin(mCameraElevation) * mCameraDistance;
	cameraPos.z = cos(mCameraElevation) * mCameraDistance;
	mCamera->setPosition(cameraPos);
	mCamera->lookAt(ofVec3f(0.0, 0.0, 0.0), ofVec3f(0.0, 1.0, 0.0));
	mCamera->setFov(20.0);
}