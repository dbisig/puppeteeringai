/** \file dab_mocap_data_serialize.h
*/

#pragma once

#include "ofxJSON.h"
#include "dab_singleton.h"
#include "dab_exception.h"

namespace dab
{

class MocapDataset;
class MocapSubjectData;
class MocapSkeleton;
class MocapData;

class MocapDataSerialize : public Singleton<MocapDataSerialize>
{
public:
	void restore(const std::string& pFileName, MocapDataset& pDataSet) throw (dab::Exception);

protected:
	void restoreSubjectData(Json::Value& pData, MocapSubjectData& pSubjectData) throw (dab::Exception);
	void restoreSkeletonData(Json::Value& pData, MocapSkeleton& pSubjectData) throw (dab::Exception);
	void restoreMocapData(Json::Value& pData, MocapData& pMocapData) throw (dab::Exception);
};

};