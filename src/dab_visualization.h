/** \file dab_visualization.h
*/

#pragma once

#include "dab_singleton.h"
#include "dab_exception.h"
#include "ofVectorMath.h"
#include <torch/script.h>

namespace dab
{

class MocapSkeleton;
class VisSkeletonPlayer;

class Visualization : public Singleton<Visualization>
{
public:
	Visualization();

	void setBasePose(torch::Tensor pBasePose);

	void showRefSequence(torch::Tensor pPoseSequence, unsigned int pStartFrame, unsigned int pFrameCount, const MocapSkeleton& pMocapSkeleton, VisSkeletonPlayer& pSkeletonPlayer) throw (dab::Exception);
	void showRefSequenceWindowed(torch::Tensor pPoseSequence, unsigned int pStartFrame, unsigned int pFrameCount, unsigned int pWindowLength, unsigned int pWindowOverlap, const MocapSkeleton& pMocapSkeleton, VisSkeletonPlayer& pSkeletonPlayer) throw (dab::Exception);
	void showRefSequenceWindowed2(torch::Tensor pPoseSequence, unsigned int pStartFrame, unsigned int pFrameCount, unsigned int pWindowLength, unsigned int pWindowOverlap, const MocapSkeleton& pMocapSkeleton, VisSkeletonPlayer& pSkeletonPlayer) throw (dab::Exception);
	void showPredSequenceWindow(torch::Tensor pPoseSequence, unsigned int pStartFrame, unsigned int pWindowLength, torch::jit::script::Module& pEncoder, torch::jit::script::Module& pDecoder, const MocapSkeleton& pMocapSkeleton, VisSkeletonPlayer& pSkeletonPlayer);
	void showPredSequence(torch::Tensor pPoseSequence, unsigned int pStartFrame, unsigned int pFrameCount, unsigned int pWindowLength, unsigned int pWindowOverlap, torch::jit::script::Module& pEncoder, torch::jit::script::Module& pDecoder, const MocapSkeleton& pMocapSkeleton, VisSkeletonPlayer& pSkeletonPlayer) throw (dab::Exception);
	void showPredSequence2(torch::Tensor pPoseSequence, unsigned int pStartFrame, unsigned int pFrameCount, unsigned int pWindowLength, unsigned int pWindowOverlap, torch::jit::script::Module& pEncoder, torch::jit::script::Module& pDecoder, const MocapSkeleton& pMocapSkeleton, VisSkeletonPlayer& pSkeletonPlayer) throw (dab::Exception);
	void showInterpolatedSequence(torch::Tensor pPoseSequence, unsigned int pStartFrame1, unsigned int pStartFrame2, unsigned int pFrameCount, unsigned int pWindowLength, unsigned int pWindowOverlap, float pStartAlpha, float pEndAlpha, torch::jit::script::Module& pEncoder, torch::jit::script::Module& pDecoder, const MocapSkeleton& pMocapSkeleton, VisSkeletonPlayer& pSkeletonPlayer) throw (dab::Exception);
	void showInterpolatedSequence2(torch::Tensor pPoseSequence, unsigned int pStartFrame1, unsigned int pStartFrame2, unsigned int pFrameCount, unsigned int pWindowLength, unsigned int pWindowOverlap, float pStartAlpha, float pEndAlpha, torch::jit::script::Module& pEncoder, torch::jit::script::Module& pDecoder, const MocapSkeleton& pMocapSkeleton, VisSkeletonPlayer& pSkeletonPlayer) throw (dab::Exception);

	void tensor2glm(torch::Tensor pPositionSequenceTensor, std::vector< std::vector< glm::vec3> >& pPositionSequenceVector ) throw (dab::Exception);
	void tensor2glm(torch::Tensor pRotationSequenceTensor, std::vector< std::vector< glm::quat> >& pRotationSequenceVector) throw (dab::Exception);
protected:

	torch::Tensor mBasePose;
};
	
};