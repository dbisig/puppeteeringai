/** \file dab_vis_skeleton.cpp
*/

#include "dab_vis_skeleton.h"
#include "dab_vis_shape.h"
#include "dab_math_vec.h"
#include "ofApp.h"
#include <memory>

using namespace dab;

#pragma mark VisSkeleton definition

float VisSkeleton::sJointRadius = 4.0;
float VisSkeleton::sEdgeRadius = 2.0;

VisSkeleton::VisSkeleton(const std::vector< std::vector<int> >& pEdgeConnectivity)
	: mEdgeConnectivity(pEdgeConnectivity)
{
	init();
}

VisSkeleton::~VisSkeleton()
{}

unsigned int 
VisSkeleton::jointCount() const
{
	return mJointShapes.size();
}

void 
VisSkeleton::setJointMaterial(const ofMaterial& pMaterial)
{
	for (auto joint : mJointShapes) joint->setMaterial(pMaterial);
}

void 
VisSkeleton::setEdgeMaterial(const ofMaterial& pMaterial)
{
	for (auto edge : mEdgeShapes) edge->setMaterial(pMaterial);
}

void
VisSkeleton::update(const std::vector<glm::vec3>& pJointPositions) throw (dab::Exception)
{
	math::VectorMath& vecMath = math::VectorMath::get();

	int jointCount = mJointShapes.size();
	int edgeCount = mEdgeShapes.size();

	if (pJointPositions.size() != jointCount) throw dab::Exception("VIS ERROR: number of joint positions doesn't match number of joints", __FILE__, __FUNCTION__, __LINE__);

	for (int jI = 0; jI < jointCount; ++jI)
	{
		mJointShapes[jI]->setTransform(pJointPositions[jI], true);

		//std::cout << "joint " << jI << " pos " << pJointPositions[jI] << "\n";
	}

	for (int jI = 0, eI = 0; jI < jointCount; ++jI)
	{
		int jointEdgeCount = mEdgeConnectivity[jI].size();

		for (int jeI = 0; jeI < jointEdgeCount; ++jeI, ++eI)
		{
			int jI2 = mEdgeConnectivity[jI][jeI];
			const glm::vec3& jP1 = pJointPositions[jI];
			const glm::vec3& jP2 = pJointPositions[jI2];
	
			glm::vec3 edgeVec = jP2 - jP1;
			float edgeLength = glm::length(edgeVec);
			glm::vec3 edgeDir = glm::normalize(edgeVec);
			glm::vec3 refDir(0.0, 1.0, 0.0);

			glm::vec3 edgePos = (jP2 + jP1) * 0.5;
			glm::quat edgeRot = vecMath.makeQuaternion(refDir, edgeDir);

			mEdgeShapes[eI]->setShapeProperties( { sEdgeRadius, edgeLength } );
			mEdgeShapes[eI]->setTransform(edgePos, edgeRot, true);
		}
	}
}

void 
VisSkeleton::draw()
{
	ofSetColor(255, 255, 255);

	int jointCount = mJointShapes.size();
	int edgeCount = mEdgeShapes.size();

	for (int eI = 0; eI < edgeCount; ++eI)
	{
		mEdgeShapes[eI]->drawFaces();
	}

	for (int jI = 0; jI < jointCount; ++jI)
	{
		mJointShapes[jI]->drawFaces();
	}
}

void
VisSkeleton::init()
{
	// add spheres for each joint
	int jointCount = mEdgeConnectivity.size();
	for (int jI = 0; jI < jointCount; ++jI)
	{
		mJointShapes.push_back(std::shared_ptr<VisShape>(new VisShape(VisSphereType, { sJointRadius })));
	}

	// add cylinders for each edge
	for (int jI = 0; jI < jointCount; ++jI)
	{
		int childCount = mEdgeConnectivity[jI].size();
		for (int cI = 0; cI < childCount; ++cI)
		{
			float edgeLength = 100.0; // fixed length, the length will be corrected in the update routine
			mEdgeShapes.push_back(std::shared_ptr<VisShape>(new VisShape(VisCylinderType, { sEdgeRadius, edgeLength })));
		}
	}
}

#pragma mark VisSkeletonPlayer implementation

float VisSkeletonPlayer::sFrameRate = 50.0;
bool VisSkeletonPlayer::sLoop = true;

VisSkeletonPlayer::VisSkeletonPlayer(const std::vector< std::vector<int> >& pEdgeConnectivity)
	: mFrameRate(sFrameRate)
	, mLoop(sLoop)
{
	init(pEdgeConnectivity);
}

VisSkeletonPlayer::~VisSkeletonPlayer()
{}

std::shared_ptr<VisSkeleton> 
VisSkeletonPlayer::skeleton()
{
	return mSkeleton;
}

void 
VisSkeletonPlayer::setPositionSequence(const std::vector< std::vector<glm::vec3> >& pPositionSequence)
{
	mPositionSequence = pPositionSequence;

	//std::cout << "VisSkeletonPlayer::setPositionSequence\n";
	//std::cout << "mPlayFrame " << mPlayFrame << "\n";
	//for (int sI = 0; sI < mPositionSequence.size(); ++sI)
	//{
	//	std::cout << mPositionSequence[sI][10].x << " ";
	//}
	//std::cout << "\n";

	//mPlayFrame = 0;
	//mPlayStartTime = static_cast<double>(ofGetElapsedTimeMillis());
}

void 
VisSkeletonPlayer::play(const std::vector< std::vector<glm::vec3> >& pPositionSequence)
{
	mPositionSequence = pPositionSequence;
	mPlayFrame = 0;
	mPlayStartTime = static_cast<double>(ofGetElapsedTimeMillis());

	try
	{
		mSkeleton->update(mPositionSequence[mPlayFrame]);
		mPlay = true;
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}
}

void 
VisSkeletonPlayer::play(torch::Tensor pPositionSequence) throw(dab::Exception)
{
	std::cout << "VisSkeletonPlayer::play(torch::Tensor pPositionSequence) begin\n";

	unsigned int jointCount = mSkeleton->jointCount();
	unsigned int jointDim = 3;

	if (pPositionSequence.dim() != 3) throw dab::Exception("VIS ERROR: positionSequence has wrong dimension", __FILE__, __FUNCTION__, __LINE__);
	if (pPositionSequence.size(1) != jointCount) throw dab::Exception("VIS ERROR: positionSequence has wrong number of joints", __FILE__, __FUNCTION__, __LINE__);
	if (pPositionSequence.size(2) != jointDim) throw dab::Exception("VIS ERROR: positionSequence has wrong joint dimension", __FILE__, __FUNCTION__, __LINE__);

	unsigned int sequenceLength = pPositionSequence.size(0);

	auto _sequence = pPositionSequence.accessor<float, 3>();

	std::vector< std::vector<glm::vec3> > jointPositions;

	for (int sI = 0; sI < sequenceLength; ++sI)
	{
		jointPositions.push_back(std::vector<glm::vec3>());
		for (int jI = 0; jI < jointCount; ++jI)
		{
			glm::vec3 jointPos(_sequence[sI][jI][0], _sequence[sI][jI][1], _sequence[sI][jI][2]);
			jointPositions[sI].push_back(jointPos);
		}
	}

	play(jointPositions);

	std::cout << "VisSkeletonPlayer::play(torch::Tensor pPositionSequence) end\n";
}

void
VisSkeletonPlayer::stop()
{
	mPlay = false;
}

void 
VisSkeletonPlayer::draw()
{
	if (mPlay == true)
	{
		double currentTime = static_cast<double>(ofGetElapsedTimeMillis());
		double elapsedTime = (currentTime - mPlayStartTime) / 1000.0;
		int nextPlayFrame = static_cast<int>(elapsedTime * mFrameRate);

		if (nextPlayFrame > mPlayFrame)
		{
			bool updateSkeleton = true;

			if (nextPlayFrame >= mPositionSequence.size())
			{
				if (mLoop == false)
				{
					updateSkeleton = false;
					mPlay = false;
				}
				nextPlayFrame = 0;
				mPlayStartTime = static_cast<double>(ofGetElapsedTimeMillis());
			}

			mPlayFrame = nextPlayFrame;

			//std::cout << "mPlayFrame " << mPlayFrame << "\n";

			if (updateSkeleton == true)
			{
				try
				{
					//std::cout << "update mPlayFrame " << mPlayFrame << " pos " << mPositionSequence[mPlayFrame][10]  << "\n";

					mSkeleton->update(mPositionSequence[mPlayFrame]);
				}
				catch (dab::Exception& e)
				{
					std::cout << e << "\n";
				}
			}
		}
	}

	mSkeleton->draw();
}

int 
VisSkeletonPlayer::playFrame() const
{
	return mPlayFrame;
}

void 
VisSkeletonPlayer::init(const std::vector< std::vector<int> >& pEdgeConnectivity)
{
	mSkeleton = std::make_shared<VisSkeleton>(pEdgeConnectivity);
}

