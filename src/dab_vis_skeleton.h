/** \file dab_vis_skeleton.h
*/

#pragma once

#include <vector>
#include <torch/script.h>
#include "ofVectorMath.h"
#include "ofMaterial.h"
#include "dab_exception.h"

namespace dab
{

class VisShape;
class MocapSkeleton;

#pragma mark VisSkeleton definition

class VisSkeleton
{
public:
	VisSkeleton(const std::vector< std::vector<int> >& pEdgeConnectivity);
	~VisSkeleton();

	unsigned int jointCount() const;

	void setJointMaterial(const ofMaterial& pMaterial);
	void setEdgeMaterial(const ofMaterial& pMaterial);

	void update(const std::vector<glm::vec3>& pJointPositions) throw (dab::Exception);

	void draw();

protected:
	static float sJointRadius;
	static float sEdgeRadius;

	void init();

	std::vector< std::vector<int> >mEdgeConnectivity;
	std::vector< std::shared_ptr<VisShape> > mJointShapes;
	std::vector< std::shared_ptr<VisShape> > mEdgeShapes;
};

#pragma mark VisSkeletonPlayer definition

class VisSkeletonPlayer
{
public:
	VisSkeletonPlayer(const std::vector< std::vector<int> >& pEdgeConnectivity);
	~VisSkeletonPlayer();

	std::shared_ptr<VisSkeleton> skeleton();

	void setPositionSequence(const std::vector< std::vector<glm::vec3> >& pPositionSequence);
	void play(const std::vector< std::vector<glm::vec3> >& pPositionSequence);
	void play(torch::Tensor pPositionSequence) throw(dab::Exception);
	void stop();
	void draw();

	int playFrame() const;

protected:
	static float sFrameRate;
	static bool sLoop;

	void init(const std::vector< std::vector<int> >& pEdgeConnectivity);

	std::shared_ptr<VisSkeleton> mSkeleton;
	std::vector< std::vector<glm::vec3> > mPositionSequence;
	float mFrameRate;
	bool mPlay;
	bool mLoop;

	int mPlayFrame;
	double mPlayStartTime;
};

};