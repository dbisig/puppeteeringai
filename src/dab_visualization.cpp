/** \file dab_visualization.cpp
*/

#include "dab_visualization.h"
#include "dab_mocap_data.h"
#include "dab_vis_skeleton.h"
#include "dab_torch_quat.h"
#include <torch/nn.h>

using namespace dab;
using namespace torch::indexing;

Visualization::Visualization()
	: mBasePose(torch::zeros({ 1 }))
{}

void
Visualization::setBasePose(torch::Tensor pBasePose)
{
	mBasePose = pBasePose;
}

void 
Visualization::showRefSequence(torch::Tensor pPoseSequence, unsigned int pStartFrame, unsigned int pFrameCount, const MocapSkeleton& pMocapSkeleton, VisSkeletonPlayer& pSkeletonPlayer) throw (dab::Exception)
{
	try
	{
		std::cout << "showRefSequenceWindow begin\n";
		std::cout << "pPoseSequence s " << pPoseSequence.sizes() << "\n";

		// ensure that pPoseSequence has format: batch_size, sequence_length, joint_count, joint_dim
		if (pPoseSequence.dim() != 4) throw dab::Exception("VIS ERROR: poseSequence has wrong number of dimensions", __FILE__, __FUNCTION__, __LINE__);

		int batchSize = pPoseSequence.size(0);
		int poseSequenceLength = pPoseSequence.size(1);
		int jointCount = pPoseSequence.size(2);
		int jointDim = pPoseSequence.size(3);

		if (jointCount != pMocapSkeleton.jointCount()) throw dab::Exception("VIS ERROR: poseSequence has wrong joint count", __FILE__, __FUNCTION__, __LINE__);
		if (jointDim != 4) throw dab::Exception("VIS ERROR: poseSequence has wrong joint dimension", __FILE__, __FUNCTION__, __LINE__);

		if (pStartFrame + pFrameCount >= poseSequenceLength) throw dab::Exception("VIS ERROR: selected frame range exceeds sequence length", __FILE__, __FUNCTION__, __LINE__);

		torch::Tensor sequenceExcerpt = pPoseSequence.index({ Slice(None), Slice(pStartFrame, pStartFrame + pFrameCount), Slice(None), Slice(None) });
		std::cout << "sequenceExcerpt s " << sequenceExcerpt.sizes() << "\n";

		torch::Tensor zeroTrajectory = torch::zeros({ batchSize, pFrameCount, 3 }, torch::kFloat32);
		std::cout << "zeroTrajectory s " << zeroTrajectory.sizes() << "\n";

		torch::Tensor positionSequenceTensor = pMocapSkeleton.forwardKinematics(sequenceExcerpt, zeroTrajectory);

		std::vector< std::vector< glm::vec3> > positionSequenceVector;

		// use only first batch for the moment
		// TODO: use multiple skeleton players and each player corresponds to a batch dim
		tensor2glm(positionSequenceTensor[0], positionSequenceVector);

		pSkeletonPlayer.stop();
		pSkeletonPlayer.play(positionSequenceVector);

		std::cout << "showRefSequenceWindow end\n";
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("VIS ERROR: failed to show reference sequence", __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void 
Visualization::showRefSequenceWindowed(torch::Tensor pPoseSequence, unsigned int pStartFrame, unsigned int pFrameCount, unsigned int pWindowLength, unsigned int pWindowOverlap, const MocapSkeleton& pMocapSkeleton, VisSkeletonPlayer& pSkeletonPlayer) throw (dab::Exception)
{
	try
	{
		//std::ofstream debugOutputFile;
		//debugOutputFile.open("slerp_log.txt");

		TorchQuat& torchquat = TorchQuat::get();

		// ensure that pPoseSequence has format: batch_size, sequence_length, joint_count, joint_dim
		if (pPoseSequence.dim() != 4) throw dab::Exception("VIS ERROR: poseSequence has wrong number of dimensions", __FILE__, __FUNCTION__, __LINE__);

		int batchSize = pPoseSequence.size(0);
		int poseSequenceLength = pPoseSequence.size(1);
		int jointCount = pPoseSequence.size(2);
		int jointDim = pPoseSequence.size(3);

		if (jointCount != pMocapSkeleton.jointCount()) throw dab::Exception("VIS ERROR: poseSequence has wrong joint count", __FILE__, __FUNCTION__, __LINE__);
		if (jointDim != 4) throw dab::Exception("VIS ERROR: poseSequence has wrong joint dimension", __FILE__, __FUNCTION__, __LINE__);

		// ensure that a proper base pose has been set
		if(mBasePose.dim() != 2 || mBasePose.size(0) != jointCount || mBasePose.size(1) != 4) throw dab::Exception("VIS ERROR: incorrect basePose", __FILE__, __FUNCTION__, __LINE__);

		// create Hann window
		torch::Tensor windowEnvelope = torch::hann_window(pWindowLength, torch::kFloat32);

		int windowCount = std::max((static_cast<int>(pFrameCount) - static_cast<int>(pWindowLength)) / static_cast<int>(pWindowOverlap), 0) + 1;
		int combinedSeqLength = (windowCount - 1) * pWindowOverlap + pWindowLength;

		torch::Tensor combinedRefSequence = mBasePose.repeat({ combinedSeqLength * batchSize, 1 }).reshape({ batchSize, combinedSeqLength, jointCount, jointDim });

		for (int wI = 0; wI < windowCount; ++wI)
		{
			int excerptStartFrame = pStartFrame + wI * pWindowOverlap;
			int excerptEndFrame = excerptStartFrame + pWindowLength;

			torch::Tensor sequenceExcerpt = pPoseSequence.index({ Slice(None), Slice(excerptStartFrame,excerptEndFrame), Slice(None), Slice(None) });

			int combinedFrame = wI * pWindowOverlap;

			for (int bI = 0; bI < batchSize; ++bI)
			{
				for (int sI = 0; sI < pWindowLength; ++sI)
				{
					for (int jI = 0; jI < jointCount; ++jI)
					{
						torch::Tensor currentQuat = combinedRefSequence[bI][combinedFrame + sI][jI];
						torch::Tensor targetQuat = sequenceExcerpt[bI][sI][jI];
						float quatMix = windowEnvelope[sI].item<float>();
						torch::Tensor mixQuat = torchquat.slerp(currentQuat, targetQuat, quatMix);
						//torch::Tensor mixQuat = torchquat.slerp_debug(wI, bI, sI, jI, debugOutputFile, currentQuat, targetQuat, quatMix);
						combinedRefSequence[bI][combinedFrame + sI][jI] = mixQuat; 
					}
				}
			}
		}

		//debugOutputFile.close();

		combinedRefSequence = combinedRefSequence.reshape({ -1, 4 });
		combinedRefSequence = torch::nn::functional::normalize(combinedRefSequence, torch::nn::functional::NormalizeFuncOptions().dim(1));
		combinedRefSequence = combinedRefSequence.reshape({ batchSize, combinedSeqLength, jointCount, jointDim });

		torch::Tensor zeroTrajectory = torch::zeros({ batchSize, combinedSeqLength, 3 }, torch::kFloat32);

		torch::Tensor positionSequenceTensor = pMocapSkeleton.forwardKinematics(combinedRefSequence, zeroTrajectory);

		std::vector< std::vector< glm::vec3> > positionSequenceVector;

		// use only first batch for the moment
		// TODO: use multiple skeleton players and each player corresponds to a batch dim
		tensor2glm(positionSequenceTensor[0], positionSequenceVector);

		pSkeletonPlayer.stop();
		pSkeletonPlayer.play(positionSequenceVector);
	}
	catch (std::exception& e)
	{
		std::cout << e.what();
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("VIS ERROR: failed to show reference sequence windowed", __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void
Visualization::showRefSequenceWindowed2(torch::Tensor pPoseSequence, unsigned int pStartFrame, unsigned int pFrameCount, unsigned int pWindowLength, unsigned int pWindowOverlap, const MocapSkeleton& pMocapSkeleton, VisSkeletonPlayer& pSkeletonPlayer) throw (dab::Exception)
{
	try
	{
		//std::ofstream debugOutputFile;
		//debugOutputFile.open("slerp2_log.txt");

		TorchQuat& torchquat = TorchQuat::get();

		// ensure that pPoseSequence has format: batch_size, sequence_length, joint_count, joint_dim
		if (pPoseSequence.dim() != 4) throw dab::Exception("VIS ERROR: poseSequence has wrong number of dimensions", __FILE__, __FUNCTION__, __LINE__);

		int batchSize = pPoseSequence.size(0);
		int poseSequenceLength = pPoseSequence.size(1);
		int jointCount = pPoseSequence.size(2);
		int jointDim = pPoseSequence.size(3);

		if (jointCount != pMocapSkeleton.jointCount()) throw dab::Exception("VIS ERROR: poseSequence has wrong joint count", __FILE__, __FUNCTION__, __LINE__);
		if (jointDim != 4) throw dab::Exception("VIS ERROR: poseSequence has wrong joint dimension", __FILE__, __FUNCTION__, __LINE__);

		// ensure that a proper base pose has been set
		if (mBasePose.dim() != 2 || mBasePose.size(0) != jointCount || mBasePose.size(1) != 4) throw dab::Exception("VIS ERROR: incorrect basePose", __FILE__, __FUNCTION__, __LINE__);

		// create Hann window
		torch::Tensor windowEnvelope = torch::hann_window(pWindowLength, torch::kFloat32);

		int windowCount = std::max((static_cast<int>(pFrameCount) - static_cast<int>(pWindowLength)) / static_cast<int>(pWindowOverlap), 0) + 1;
		int combinedSeqLength = (windowCount - 1) * pWindowOverlap + pWindowLength;

		torch::Tensor combinedRefSequence = mBasePose.repeat({ combinedSeqLength * batchSize, 1 }).reshape({ batchSize, combinedSeqLength, jointCount, jointDim });

		for (int wI = 0; wI < windowCount; ++wI)
		{
			int excerptStartFrame = pStartFrame + wI * pWindowOverlap;
			int excerptEndFrame = excerptStartFrame + pWindowLength;

			torch::Tensor sequenceExcerpt = pPoseSequence.index({ Slice(None), Slice(excerptStartFrame,excerptEndFrame), Slice(None), Slice(None) });

			int combinedFrame = wI * pWindowOverlap;

			for (int sI = 0; sI < pWindowLength; ++sI)
			{

				torch::Tensor currentQuat = combinedRefSequence.index({ Slice(None), combinedFrame + sI, Slice(None) });
				torch::Tensor targetQuat = sequenceExcerpt.index({ Slice(None), sI, Slice(None) });
				torch::Tensor quatMix = windowEnvelope[sI].repeat({ batchSize, jointCount });
				torch::Tensor mixQuat = torchquat.slerp2(currentQuat, targetQuat, quatMix);

				combinedRefSequence.index({ Slice(None), combinedFrame + sI, Slice(None) }) = mixQuat;
			}
		}

		//debugOutputFile.close();

		combinedRefSequence = combinedRefSequence.reshape({ -1, 4 });
		combinedRefSequence = torch::nn::functional::normalize(combinedRefSequence, torch::nn::functional::NormalizeFuncOptions().dim(1));
		combinedRefSequence = combinedRefSequence.reshape({ batchSize, combinedSeqLength, jointCount, jointDim });

		torch::Tensor zeroTrajectory = torch::zeros({ batchSize, combinedSeqLength, 3 }, torch::kFloat32);

		torch::Tensor positionSequenceTensor = pMocapSkeleton.forwardKinematics(combinedRefSequence, zeroTrajectory);

		std::vector< std::vector< glm::vec3> > positionSequenceVector;

		// use only first batch for the moment
		// TODO: use multiple skeleton players and each player corresponds to a batch dim
		tensor2glm(positionSequenceTensor[0], positionSequenceVector);

		pSkeletonPlayer.stop();
		pSkeletonPlayer.play(positionSequenceVector);
	}
	catch (std::exception& e)
	{
		std::cout << e.what();
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("VIS ERROR: failed to show reference sequence windowed", __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void 
Visualization::showPredSequenceWindow(torch::Tensor pPoseSequence, unsigned int pStartFrame, unsigned int pWindowLength, torch::jit::script::Module& pEncoder, torch::jit::script::Module& pDecoder, const MocapSkeleton& pMocapSkeleton, VisSkeletonPlayer& pSkeletonPlayer)
{
	try
	{
		std::cout << "showPredSequenceWindow begin\n";

		// ensure that pPoseSequence has format: batch_size, sequence_length, joint_count, joint_dim
		if (pPoseSequence.dim() != 4) throw dab::Exception("VIS ERROR: poseSequence has wrong number of dimensions", __FILE__, __FUNCTION__, __LINE__);

		int batchSize = pPoseSequence.size(0);
		int poseSequenceLength = pPoseSequence.size(1);
		int jointCount = pPoseSequence.size(2);
		int jointDim = pPoseSequence.size(3);

		if (jointCount != pMocapSkeleton.jointCount()) throw dab::Exception("VIS ERROR: poseSequence has wrong joint count", __FILE__, __FUNCTION__, __LINE__);
		if (jointDim != 4) throw dab::Exception("VIS ERROR: poseSequence has wrong joint dimension", __FILE__, __FUNCTION__, __LINE__);

		if (pStartFrame + pWindowLength >= poseSequenceLength) throw dab::Exception("VIS ERROR: selected frame range exceeds sequence length", __FILE__, __FUNCTION__, __LINE__);

		torch::Tensor sequenceExcerpt = pPoseSequence.index({ Slice(None), Slice(pStartFrame, pStartFrame + pWindowLength), Slice(None), Slice(None) });
		sequenceExcerpt = sequenceExcerpt.reshape({ batchSize, pWindowLength, -1 });
		
		torch::Tensor sequenceEncoded = pEncoder.forward({ sequenceExcerpt }).toTensor();
		torch::Tensor sequenceDecoded = pDecoder.forward({ sequenceEncoded }).toTensor();
		
		sequenceDecoded = sequenceDecoded.reshape({ batchSize, pWindowLength, jointCount, jointDim });

		torch::Tensor zeroTrajectory = torch::zeros({ batchSize, pWindowLength, 3 }, torch::kFloat32);
		torch::Tensor positionSequenceTensor = pMocapSkeleton.forwardKinematics(sequenceDecoded, zeroTrajectory);

		std::vector< std::vector< glm::vec3> > positionSequenceVector;

		// use only first batch for the moment
		// TODO: use multiple skeleton players and each player corresponds to a batch dim
		tensor2glm(positionSequenceTensor[0], positionSequenceVector);

		pSkeletonPlayer.stop();
		pSkeletonPlayer.play(positionSequenceVector);

		std::cout << "showPredSequenceWindow end\n";
	}
	catch (std::exception& e)
	{
		std::cout << e.what();
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("VIS ERROR: failed to show predicted sequence window", __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void 
Visualization::showPredSequence(torch::Tensor pPoseSequence, unsigned int pStartFrame, unsigned int pFrameCount, unsigned int pWindowLength, unsigned int pWindowOverlap, torch::jit::script::Module& pEncoder, torch::jit::script::Module& pDecoder, const MocapSkeleton& pMocapSkeleton, VisSkeletonPlayer& pSkeletonPlayer) throw (dab::Exception)
{
	try
	{
		TorchQuat& torchquat = TorchQuat::get();

		// ensure that pPoseSequence has format: batch_size, sequence_length, joint_count, joint_dim
		if (pPoseSequence.dim() != 4) throw dab::Exception("VIS ERROR: poseSequence has wrong number of dimensions", __FILE__, __FUNCTION__, __LINE__);

		int batchSize = pPoseSequence.size(0);
		int poseSequenceLength = pPoseSequence.size(1);
		int jointCount = pPoseSequence.size(2);
		int jointDim = pPoseSequence.size(3);

		if (jointCount != pMocapSkeleton.jointCount()) throw dab::Exception("VIS ERROR: poseSequence has wrong joint count", __FILE__, __FUNCTION__, __LINE__);
		if (jointDim != 4) throw dab::Exception("VIS ERROR: poseSequence has wrong joint dimension", __FILE__, __FUNCTION__, __LINE__);

		// ensure that a proper base pose has been set
		if (mBasePose.dim() != 2 || mBasePose.size(0) != jointCount || mBasePose.size(1) != 4) throw dab::Exception("VIS ERROR: incorrect basePose", __FILE__, __FUNCTION__, __LINE__);

		// create Hann window
		torch::Tensor windowEnvelope = torch::hann_window(pWindowLength, torch::kFloat32);

		// prepare output sequence
		int windowCount = std::max((static_cast<int>(pFrameCount) - static_cast<int>(pWindowLength)) / static_cast<int>(pWindowOverlap), 0) + 1;
		int combinedSeqLength = (windowCount - 1) * pWindowOverlap + pWindowLength;

		std::cout << "windowCount " << windowCount << "\n";

		torch::Tensor combinedPredSequence = mBasePose.repeat({ combinedSeqLength * batchSize, 1 }).reshape({ batchSize, combinedSeqLength, jointCount, jointDim });
	
		for (int wI = 0; wI < windowCount; ++wI)
		{
			int excerptStartFrame = pStartFrame + wI * pWindowOverlap;
			int excerptEndFrame = excerptStartFrame + pWindowLength;

			torch::Tensor sequenceExcerpt = pPoseSequence.index({ Slice(None), Slice(excerptStartFrame,excerptEndFrame), Slice(None), Slice(None) });
			sequenceExcerpt = sequenceExcerpt.reshape({ batchSize, pWindowLength, -1 });

			torch::Tensor sequenceEncoded = pEncoder.forward({ sequenceExcerpt }).toTensor();
			torch::Tensor sequenceDecoded = pDecoder.forward({ sequenceEncoded }).toTensor();

			sequenceDecoded = sequenceDecoded.reshape({ batchSize, pWindowLength, jointCount, jointDim });

			int combinedFrame = wI * pWindowOverlap;

			for (int bI = 0; bI < batchSize; ++bI)
			{
				for (int sI = 0; sI < pWindowLength; ++sI)
				{
					for (int jI = 0; jI < jointCount; ++jI)
					{
						torch::Tensor currentQuat = combinedPredSequence[bI][combinedFrame + sI][jI];
						torch::Tensor targetQuat = sequenceDecoded[bI][sI][jI];
						float quatMix = windowEnvelope[sI].item<float>();
						torch::Tensor mixQuat = torchquat.slerp(currentQuat, targetQuat, quatMix);
						combinedPredSequence[bI][combinedFrame + sI][jI] = mixQuat;
					}
				}
			}
		}

		combinedPredSequence = combinedPredSequence.reshape({ -1, 4 });
		combinedPredSequence = torch::nn::functional::normalize(combinedPredSequence, torch::nn::functional::NormalizeFuncOptions().dim(1));
		combinedPredSequence = combinedPredSequence.reshape({ batchSize, combinedSeqLength, jointCount, jointDim });

		torch::Tensor zeroTrajectory = torch::zeros({ batchSize, combinedSeqLength, 3 }, torch::kFloat32);

		torch::Tensor positionSequenceTensor = pMocapSkeleton.forwardKinematics(combinedPredSequence, zeroTrajectory);

		std::vector< std::vector< glm::vec3> > positionSequenceVector;

		// use only first batch for the moment
		// TODO: use multiple skeleton players and each player corresponds to a batch dim
		tensor2glm(positionSequenceTensor[0], positionSequenceVector);

		pSkeletonPlayer.stop();
		pSkeletonPlayer.play(positionSequenceVector);
	}
	catch (std::exception& e)
	{
		std::cout << e.what();
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("VIS ERROR: failed to show predicted sequence window", __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void
Visualization::showPredSequence2(torch::Tensor pPoseSequence, unsigned int pStartFrame, unsigned int pFrameCount, unsigned int pWindowLength, unsigned int pWindowOverlap, torch::jit::script::Module& pEncoder, torch::jit::script::Module& pDecoder, const MocapSkeleton& pMocapSkeleton, VisSkeletonPlayer& pSkeletonPlayer) throw (dab::Exception)
{
	try
	{
		TorchQuat& torchquat = TorchQuat::get();

		// ensure that pPoseSequence has format: batch_size, sequence_length, joint_count, joint_dim
		if (pPoseSequence.dim() != 4) throw dab::Exception("VIS ERROR: poseSequence has wrong number of dimensions", __FILE__, __FUNCTION__, __LINE__);

		int batchSize = pPoseSequence.size(0);
		int poseSequenceLength = pPoseSequence.size(1);
		int jointCount = pPoseSequence.size(2);
		int jointDim = pPoseSequence.size(3);

		if (jointCount != pMocapSkeleton.jointCount()) throw dab::Exception("VIS ERROR: poseSequence has wrong joint count", __FILE__, __FUNCTION__, __LINE__);
		if (jointDim != 4) throw dab::Exception("VIS ERROR: poseSequence has wrong joint dimension", __FILE__, __FUNCTION__, __LINE__);

		// ensure that a proper base pose has been set
		if (mBasePose.dim() != 2 || mBasePose.size(0) != jointCount || mBasePose.size(1) != 4) throw dab::Exception("VIS ERROR: incorrect basePose", __FILE__, __FUNCTION__, __LINE__);

		// create Hann window
		torch::Tensor windowEnvelope = torch::hann_window(pWindowLength, torch::kFloat32);

		// prepare output sequence
		int windowCount = std::max((static_cast<int>(pFrameCount) - static_cast<int>(pWindowLength)) / static_cast<int>(pWindowOverlap), 0) + 1;
		int combinedSeqLength = (windowCount - 1) * pWindowOverlap + pWindowLength;

		std::cout << "windowCount " << windowCount << "\n";
		std::cout << "combinedSeqLength " << combinedSeqLength << "\n";

		torch::Tensor combinedPredSequence = mBasePose.repeat({ combinedSeqLength * batchSize, 1 }).reshape({ batchSize, combinedSeqLength, jointCount, jointDim });

		for (int wI = 0; wI < windowCount; ++wI)
		{
			int excerptStartFrame = pStartFrame + wI * pWindowOverlap;
			int excerptEndFrame = excerptStartFrame + pWindowLength;

			torch::Tensor sequenceExcerpt = pPoseSequence.index({ Slice(None), Slice(excerptStartFrame,excerptEndFrame), Slice(None), Slice(None) });
			sequenceExcerpt = sequenceExcerpt.reshape({ batchSize, pWindowLength, -1 });

			torch::Tensor sequenceEncoded = pEncoder.forward({ sequenceExcerpt }).toTensor();
			torch::Tensor sequenceDecoded = pDecoder.forward({ sequenceEncoded }).toTensor();

			sequenceDecoded = sequenceDecoded.reshape({ batchSize, pWindowLength, jointCount, jointDim });

			int combinedFrame = wI * pWindowOverlap;

			for (int sI = 0; sI < pWindowLength; ++sI)
			{
				torch::Tensor currentQuat = combinedPredSequence.index({ Slice(None), combinedFrame + sI, Slice(None) });
				torch::Tensor targetQuat = sequenceExcerpt.index({ Slice(None), sI, Slice(None) });
				torch::Tensor quatMix = windowEnvelope[sI].repeat({ batchSize, jointCount });
				torch::Tensor mixQuat = torchquat.slerp2(currentQuat, targetQuat, quatMix);

				combinedPredSequence.index({ Slice(None), combinedFrame + sI, Slice(None) }) = mixQuat;
			}
		}

		combinedPredSequence = combinedPredSequence.reshape({ -1, 4 });
		combinedPredSequence = torch::nn::functional::normalize(combinedPredSequence, torch::nn::functional::NormalizeFuncOptions().dim(1));
		combinedPredSequence = combinedPredSequence.reshape({ batchSize, combinedSeqLength, jointCount, jointDim });

		torch::Tensor zeroTrajectory = torch::zeros({ batchSize, combinedSeqLength, 3 }, torch::kFloat32);

		torch::Tensor positionSequenceTensor = pMocapSkeleton.forwardKinematics(combinedPredSequence, zeroTrajectory);

		std::vector< std::vector< glm::vec3> > positionSequenceVector;

		// use only first batch for the moment
		// TODO: use multiple skeleton players and each player corresponds to a batch dim
		tensor2glm(positionSequenceTensor[0], positionSequenceVector);

		pSkeletonPlayer.stop();
		pSkeletonPlayer.play(positionSequenceVector);
	}
	catch (std::exception& e)
	{
		std::cout << e.what();
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("VIS ERROR: failed to show predicted sequence window", __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void 
Visualization::showInterpolatedSequence(torch::Tensor pPoseSequence, unsigned int pStartFrame1, unsigned int pStartFrame2, unsigned int pFrameCount, unsigned int pWindowLength, unsigned int pWindowOverlap, float pStartAlpha, float pEndAlpha, torch::jit::script::Module& pEncoder, torch::jit::script::Module& pDecoder, const MocapSkeleton& pMocapSkeleton, VisSkeletonPlayer& pSkeletonPlayer) throw (dab::Exception)
{
	try
	{
		TorchQuat& torchquat = TorchQuat::get();

		// ensure that pPoseSequence has format: batch_size, sequence_length, joint_count, joint_dim
		if (pPoseSequence.dim() != 4) throw dab::Exception("VIS ERROR: poseSequence has wrong number of dimensions", __FILE__, __FUNCTION__, __LINE__);

		int batchSize = pPoseSequence.size(0);
		int poseSequenceLength = pPoseSequence.size(1);
		int jointCount = pPoseSequence.size(2);
		int jointDim = pPoseSequence.size(3);

		if (jointCount != pMocapSkeleton.jointCount()) throw dab::Exception("VIS ERROR: poseSequence has wrong joint count", __FILE__, __FUNCTION__, __LINE__);
		if (jointDim != 4) throw dab::Exception("VIS ERROR: poseSequence has wrong joint dimension", __FILE__, __FUNCTION__, __LINE__);

		// ensure that a proper base pose has been set
		if (mBasePose.dim() != 2 || mBasePose.size(0) != jointCount || mBasePose.size(1) != 4) throw dab::Exception("VIS ERROR: incorrect basePose", __FILE__, __FUNCTION__, __LINE__);

		// create Hann window
		torch::Tensor windowEnvelope = torch::hann_window(pWindowLength, torch::kFloat32);

		// prepare output sequence
		int windowCount = std::max((static_cast<int>(pFrameCount) - static_cast<int>(pWindowLength)) / static_cast<int>(pWindowOverlap), 0) + 1;
		int combinedSeqLength = (windowCount - 1) * pWindowOverlap + pWindowLength;

		torch::Tensor combinedPredSequence = mBasePose.repeat({ combinedSeqLength * batchSize, 1 }).reshape({ batchSize, combinedSeqLength, jointCount, jointDim });

		for (int wI = 0; wI < windowCount; ++wI)
		{
			int excerpt1StartFrame = pStartFrame1 + wI * pWindowOverlap;
			int excerpt1EndFrame = excerpt1StartFrame + pWindowLength;
			torch::Tensor sequence1Excerpt = pPoseSequence.index({ Slice(None), Slice(excerpt1StartFrame,excerpt1EndFrame), Slice(None), Slice(None) });
			sequence1Excerpt = sequence1Excerpt.reshape({ batchSize, pWindowLength, -1 });

			int excerpt2StartFrame = pStartFrame2 + wI * pWindowOverlap;
			int excerpt2EndFrame = excerpt2StartFrame + pWindowLength;
			torch::Tensor sequence2Excerpt = pPoseSequence.index({ Slice(None), Slice(excerpt2StartFrame,excerpt2EndFrame), Slice(None), Slice(None) });
			sequence2Excerpt = sequence2Excerpt.reshape({ batchSize, pWindowLength, -1 });

			torch::Tensor sequence1Encoded = pEncoder.forward({ sequence1Excerpt }).toTensor();
			torch::Tensor sequence2Encoded = pEncoder.forward({ sequence2Excerpt }).toTensor();

			float alpha = pStartAlpha + (pEndAlpha - pStartAlpha) * static_cast<float>(wI) / static_cast<float>(windowCount - 1);
			torch::Tensor mixedEncoding = sequence1Encoded * alpha + sequence2Encoded * (1.0 - alpha);

			torch::Tensor sequenceDecoded = pDecoder.forward({ mixedEncoding }).toTensor();
			sequenceDecoded = sequenceDecoded.reshape({ batchSize, pWindowLength, jointCount, jointDim });

			int combinedFrame = wI * pWindowOverlap;

			for (int bI = 0; bI < batchSize; ++bI)
			{
				for (int sI = 0; sI < pWindowLength; ++sI)
				{
					for (int jI = 0; jI < jointCount; ++jI)
					{
						torch::Tensor currentQuat = combinedPredSequence[bI][combinedFrame + sI][jI];
						torch::Tensor targetQuat = sequenceDecoded[bI][sI][jI];
						float quatMix = windowEnvelope[sI].item<float>();
						torch::Tensor mixQuat = torchquat.slerp(currentQuat, targetQuat, quatMix);
						combinedPredSequence[bI][combinedFrame + sI][jI] = mixQuat;
					}
				}
			}
		}

		combinedPredSequence = combinedPredSequence.reshape({ -1, 4 });
		combinedPredSequence = torch::nn::functional::normalize(combinedPredSequence, torch::nn::functional::NormalizeFuncOptions().dim(1));
		combinedPredSequence = combinedPredSequence.reshape({ batchSize, combinedSeqLength, jointCount, jointDim });

		torch::Tensor zeroTrajectory = torch::zeros({ batchSize, combinedSeqLength, 3 }, torch::kFloat32);

		torch::Tensor positionSequenceTensor = pMocapSkeleton.forwardKinematics(combinedPredSequence, zeroTrajectory);

		std::vector< std::vector< glm::vec3> > positionSequenceVector;

		// use only first batch for the moment
		// TODO: use multiple skeleton players and each player corresponds to a batch dim
		tensor2glm(positionSequenceTensor[0], positionSequenceVector);

		pSkeletonPlayer.stop();
		pSkeletonPlayer.play(positionSequenceVector);
	}
	catch (std::exception& e)
	{
		std::cout << e.what();
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("VIS ERROR: failed to show predicted sequence window", __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void
Visualization::showInterpolatedSequence2(torch::Tensor pPoseSequence, unsigned int pStartFrame1, unsigned int pStartFrame2, unsigned int pFrameCount, unsigned int pWindowLength, unsigned int pWindowOverlap, float pStartAlpha, float pEndAlpha, torch::jit::script::Module& pEncoder, torch::jit::script::Module& pDecoder, const MocapSkeleton& pMocapSkeleton, VisSkeletonPlayer& pSkeletonPlayer) throw (dab::Exception)
{
	try
	{
		TorchQuat& torchquat = TorchQuat::get();

		// ensure that pPoseSequence has format: batch_size, sequence_length, joint_count, joint_dim
		if (pPoseSequence.dim() != 4) throw dab::Exception("VIS ERROR: poseSequence has wrong number of dimensions", __FILE__, __FUNCTION__, __LINE__);

		int batchSize = pPoseSequence.size(0);
		int poseSequenceLength = pPoseSequence.size(1);
		int jointCount = pPoseSequence.size(2);
		int jointDim = pPoseSequence.size(3);

		if (jointCount != pMocapSkeleton.jointCount()) throw dab::Exception("VIS ERROR: poseSequence has wrong joint count", __FILE__, __FUNCTION__, __LINE__);
		if (jointDim != 4) throw dab::Exception("VIS ERROR: poseSequence has wrong joint dimension", __FILE__, __FUNCTION__, __LINE__);

		// ensure that a proper base pose has been set
		if (mBasePose.dim() != 2 || mBasePose.size(0) != jointCount || mBasePose.size(1) != 4) throw dab::Exception("VIS ERROR: incorrect basePose", __FILE__, __FUNCTION__, __LINE__);

		// create Hann window
		torch::Tensor windowEnvelope = torch::hann_window(pWindowLength, torch::kFloat32);

		// prepare output sequence
		int windowCount = std::max((static_cast<int>(pFrameCount) - static_cast<int>(pWindowLength)) / static_cast<int>(pWindowOverlap), 0) + 1;
		int combinedSeqLength = (windowCount - 1) * pWindowOverlap + pWindowLength;

		torch::Tensor combinedPredSequence = mBasePose.repeat({ combinedSeqLength * batchSize, 1 }).reshape({ batchSize, combinedSeqLength, jointCount, jointDim });

		for (int wI = 0; wI < windowCount; ++wI)
		{
			int excerpt1StartFrame = pStartFrame1 + wI * pWindowOverlap;
			int excerpt1EndFrame = excerpt1StartFrame + pWindowLength;
			torch::Tensor sequence1Excerpt = pPoseSequence.index({ Slice(None), Slice(excerpt1StartFrame,excerpt1EndFrame), Slice(None), Slice(None) });
			sequence1Excerpt = sequence1Excerpt.reshape({ batchSize, pWindowLength, -1 });

			int excerpt2StartFrame = pStartFrame2 + wI * pWindowOverlap;
			int excerpt2EndFrame = excerpt2StartFrame + pWindowLength;
			torch::Tensor sequence2Excerpt = pPoseSequence.index({ Slice(None), Slice(excerpt2StartFrame,excerpt2EndFrame), Slice(None), Slice(None) });
			sequence2Excerpt = sequence2Excerpt.reshape({ batchSize, pWindowLength, -1 });

			torch::Tensor sequence1Encoded = pEncoder.forward({ sequence1Excerpt }).toTensor();
			torch::Tensor sequence2Encoded = pEncoder.forward({ sequence2Excerpt }).toTensor();

			float alpha = pStartAlpha + (pEndAlpha - pStartAlpha) * static_cast<float>(wI) / static_cast<float>(windowCount - 1);
			torch::Tensor mixedEncoding = sequence1Encoded * alpha + sequence2Encoded * (1.0 - alpha);

			torch::Tensor sequenceDecoded = pDecoder.forward({ mixedEncoding }).toTensor();
			sequenceDecoded = sequenceDecoded.reshape({ batchSize, pWindowLength, jointCount, jointDim });

			int combinedFrame = wI * pWindowOverlap;

			for (int sI = 0; sI < pWindowLength; ++sI)
			{
				torch::Tensor currentQuat = combinedPredSequence.index({ Slice(None), combinedFrame + sI, Slice(None) });
				torch::Tensor targetQuat = sequenceDecoded.index({ Slice(None), sI, Slice(None) });
				torch::Tensor quatMix = windowEnvelope[sI].repeat({ batchSize, jointCount });
				torch::Tensor mixQuat = torchquat.slerp2(currentQuat, targetQuat, quatMix);

				combinedPredSequence.index({ Slice(None), combinedFrame + sI, Slice(None) }) = mixQuat;
			}
		}

		combinedPredSequence = combinedPredSequence.reshape({ -1, 4 });
		combinedPredSequence = torch::nn::functional::normalize(combinedPredSequence, torch::nn::functional::NormalizeFuncOptions().dim(1));
		combinedPredSequence = combinedPredSequence.reshape({ batchSize, combinedSeqLength, jointCount, jointDim });

		torch::Tensor zeroTrajectory = torch::zeros({ batchSize, combinedSeqLength, 3 }, torch::kFloat32);

		torch::Tensor positionSequenceTensor = pMocapSkeleton.forwardKinematics(combinedPredSequence, zeroTrajectory);

		std::vector< std::vector< glm::vec3> > positionSequenceVector;

		// use only first batch for the moment
		// TODO: use multiple skeleton players and each player corresponds to a batch dim
		tensor2glm(positionSequenceTensor[0], positionSequenceVector);

		pSkeletonPlayer.stop();
		pSkeletonPlayer.play(positionSequenceVector);
	}
	catch (std::exception& e)
	{
		std::cout << e.what();
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("VIS ERROR: failed to show predicted sequence window", __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void 
Visualization::tensor2glm(torch::Tensor pPositionSequenceTensor, std::vector< std::vector< glm::vec3> >& pPositionSequenceVector) throw (dab::Exception)
{	
	// ensure that pPositionSequenceTensor has format: sequence_length, joint_count, joint_dim
	if (pPositionSequenceTensor.dim() != 3) throw dab::Exception("VIS ERROR: positionSequenceTensor has wrong number of dimensions", __FILE__, __FUNCTION__, __LINE__);

	unsigned int sequenceLength = pPositionSequenceTensor.size(0);
	unsigned int jointCount = pPositionSequenceTensor.size(1);
	unsigned int jointDim = pPositionSequenceTensor.size(2);

	if (jointDim != 3) throw dab::Exception("VIS ERROR: positionSequence has wrong joint dimension", __FILE__, __FUNCTION__, __LINE__);

	auto _sequence = pPositionSequenceTensor.accessor<float, 3>();

	pPositionSequenceVector.clear();

	for (int sI = 0; sI < sequenceLength; ++sI)
	{
		pPositionSequenceVector.push_back(std::vector<glm::vec3>());
		for (int jI = 0; jI < jointCount; ++jI)
		{
			glm::vec3 jointPos(_sequence[sI][jI][0], _sequence[sI][jI][1], _sequence[sI][jI][2]);
			pPositionSequenceVector[sI].push_back(jointPos);
		}
	}
}

void 
Visualization::tensor2glm(torch::Tensor pRotationSequenceTensor, std::vector< std::vector< glm::quat> >& pRotationSequenceVector) throw (dab::Exception)
{
	// ensure that pPositionSequenceTensor has format: sequence_length, joint_count, joint_dim
	if (pRotationSequenceTensor.dim() != 3) throw dab::Exception("VIS ERROR: rotationSequenceTensor has wrong number of dimensions", __FILE__, __FUNCTION__, __LINE__);

	unsigned int sequenceLength = pRotationSequenceTensor.size(0);
	unsigned int jointCount = pRotationSequenceTensor.size(1);
	unsigned int jointDim = pRotationSequenceTensor.size(2);

	if (jointDim != 4) throw dab::Exception("VIS ERROR: positionSequence has wrong joint dimension", __FILE__, __FUNCTION__, __LINE__);

	auto _sequence = pRotationSequenceTensor.accessor<float, 3>();

	pRotationSequenceVector.clear();

	for (int sI = 0; sI < sequenceLength; ++sI)
	{
		pRotationSequenceVector.push_back(std::vector<glm::quat>());
		for (int jI = 0; jI < jointCount; ++jI)
		{
			glm::quat jointRot(_sequence[sI][jI][0], _sequence[sI][jI][1], _sequence[sI][jI][2], _sequence[sI][jI][3]);
			pRotationSequenceVector[sI].push_back(jointRot);
		}
	}
}
