/** \file dab_torch_quat.cpp
*/

#include "dab_torch_quat.h"
#include "dab_exception.h"
#include <torch/nn.h>
#include <algorithm> 

using namespace dab;
using namespace torch::indexing;

torch::Tensor
TorchQuat::qmul(torch::Tensor q, torch::Tensor r)
{
	/*
	Multiply quaternion(s) q with quaternion(s) s
	Expects two equally - sized tensors of shape(*, 4), where * denotes any number of dimensions
	Returns q*r as a tensor of shape(*, 4)
	*/

	assert(q.size(q.dim() - 1) == 4);
	assert(r.size(r.dim() - 1) == 4);

	std::vector<int64_t> original_shape = q.sizes().vec();

	torch::Tensor terms = torch::bmm(r.view({ -1, 4, 1 }), q.view({ -1, 1, 4 }));
	torch::Tensor w = terms.index({ Slice(None), 0, 0 }) - terms.index({ Slice(None), 1, 1 }) - terms.index({ Slice(None), 2, 2 }) - terms.index({ Slice(None), 3, 3 });
	torch::Tensor x = terms.index({ Slice(None), 0, 1 }) + terms.index({ Slice(None), 1, 0 }) - terms.index({ Slice(None), 2, 3 }) + terms.index({ Slice(None), 3, 2 });
	torch::Tensor y = terms.index({ Slice(None), 0, 2 }) + terms.index({ Slice(None), 1, 3 }) + terms.index({ Slice(None), 2, 0 }) - terms.index({ Slice(None), 3, 1 });
	torch::Tensor z = terms.index({ Slice(None), 0, 3 }) - terms.index({ Slice(None), 1, 2 }) + terms.index({ Slice(None), 2, 1 }) + terms.index({ Slice(None), 3, 0 });

	return torch::stack({ w, x, y, z }, 1).view(original_shape);
}

torch::Tensor
TorchQuat::qrot(torch::Tensor q, torch::Tensor v)
{
	/*
	Rotate vector(s) v about the rotation described by quaternion(s) q.
	Expects a tensor of shape(*, 4) for q and a tensor of shape(*, 3) for v,
	where * denotes any number of dimensions.
	Returns a tensor of shape(*, 3).
	*/
	assert(q.size(q.dim() - 1) == 4);
	assert(v.size(v.dim() - 1) == 3);
	assert(v.dim() == v.dim());
	for (int d = 0; d < q.dim() - 1; ++d) assert(q.size(d) == v.dim(d));

	std::vector<int64_t> original_shape = v.sizes().vec();
	q  = q.view({ -1, 4 });
	v = v.view({ -1, 3 });

	torch::Tensor qvec = q.index({ Slice(None), Slice(1, None) });
	torch::Tensor uv = torch::cross(qvec, v, 1);
	torch::Tensor uuv = torch::cross(qvec, uv, 1);

	return (v + (q.index({ Slice(None), Slice(None, 1) }) * uv + uuv) * 2.0).view(original_shape);
}

torch::Tensor
TorchQuat::qeuler(torch::Tensor q, const std::string& order, float epsilon)
{
	/*
	Convert quaternion(s) q to Euler angles.
	Expects a tensor of shape(*, 4), where * denotes any number of dimensions.
	Returns a tensor of shape(*, 3).
	*/

	assert(q.size(q.dim() - 1) == 4);

	std::vector<int64_t> original_shape = q.sizes().vec();
	original_shape[original_shape.size() - 1] = 3;

	q = q.view({ -1, 4 });

	torch::Tensor q0 = q.index({ Slice(None), 0 });
	torch::Tensor q1 = q.index({ Slice(None), 1 });
	torch::Tensor q2 = q.index({ Slice(None), 2 });
	torch::Tensor q3 = q.index({ Slice(None), 3 });

	torch::Tensor x = torch::empty({ 1 });
	torch::Tensor y = torch::empty({ 1 });
	torch::Tensor z = torch::empty({ 1 });

	if (order == "xyz")
	{
		x = torch::atan2(2 * (q0 * q1 - q2 * q3), 1 - 2 * (q1 * q1 + q2 * q2));
		y = torch::asin(torch::clamp(2 * (q1 * q3 + q0 * q2), -1 + epsilon, 1 - epsilon));
		z = torch::atan2(2 * (q0 * q3 - q1 * q2), 1 - 2 * (q2 * q2 + q3 * q3));
	}
	else if (order == "yzx")
	{
		x = torch::atan2(2 * (q0 * q1 - q2 * q3), 1 - 2 * (q1 * q1 + q3 * q3));
		y = torch::atan2(2 * (q0 * q2 - q1 * q3), 1 - 2 * (q2 * q2 + q3 * q3));
		z = torch::asin(torch::clamp(2 * (q1 * q2 + q0 * q3), -1 + epsilon, 1 - epsilon));
	}
	else if (order == "zxy")
	{
		x = torch::asin(torch::clamp(2 * (q0 * q1 + q2 * q3), -1 + epsilon, 1 - epsilon));
		y = torch::atan2(2 * (q0 * q2 - q1 * q3), 1 - 2 * (q1 * q1 + q2 * q2));
		z = torch::atan2(2 * (q0 * q3 - q1 * q2), 1 - 2 * (q1 * q1 + q3 * q3));
	}
	else if (order == "xzy")
	{
		x = torch::atan2(2 * (q0 * q1 + q2 * q3), 1 - 2 * (q1 * q1 + q3 * q3));
		y = torch::atan2(2 * (q0 * q2 + q1 * q3), 1 - 2 * (q2 * q2 + q3 * q3));
		z = torch::asin(torch::clamp(2 * (q0 * q3 - q1 * q2), -1 + epsilon, 1 - epsilon));
	}
	else if (order == "yxz")
	{
		x = torch::asin(torch::clamp(2 * (q0 * q1 - q2 * q3), -1 + epsilon, 1 - epsilon));
		y = torch::atan2(2 * (q1 * q3 + q0 * q2), 1 - 2 * (q1 * q1 + q2 * q2));
		z = torch::atan2(2 * (q1 * q2 + q0 * q3), 1 - 2 * (q1 * q1 + q3 * q3));
	}
	else if (order == "zyx")
	{
		x = torch::atan2(2 * (q0 * q1 + q2 * q3), 1 - 2 * (q1 * q1 + q2 * q2));
		y = torch::asin(torch::clamp(2 * (q0 * q2 - q1 * q3), -1 + epsilon, 1 - epsilon));
		z = torch::atan2(2 * (q0 * q3 + q1 * q2), 1 - 2 * (q2 * q2 + q3 * q3));
	}

	return torch::stack({ x, y, z }, 1).view(original_shape);
}

torch::Tensor
TorchQuat::slerp(torch::Tensor q0, torch::Tensor q1, float amount)
{
	assert(q0.dim() == 1 && q0.size(0) == 4);
	assert(q1.dim() == 1 && q1.size(0) == 4);

	//std::cout << "q0\n" << q0 << "\n";
	//std::cout << "q1\n" << q1 << "\n";
	//std::cout << "a\n" << amount << "\n";

	// Ensure quaternion inputs are unit quaternions and 0 <= amount <= 1	
	q0 = torch::nn::functional::normalize(q0, torch::nn::functional::NormalizeFuncOptions().dim(0));
	q1 = torch::nn::functional::normalize(q1, torch::nn::functional::NormalizeFuncOptions().dim(0));

	amount = std::max(std::min(amount, 1.0f), 0.0f);

	torch::Tensor dot = torch::dot(q0, q1);

	/*
	If the dot product is negative, slerp won't take the shorter path.
	Note that v1 and -v1 are equivalent when the negation is applied to all four components.
	Fix by reversing one quaternion
	*/

	if (dot.item<float>() < 0.0)
	{
		//std::cout << "dot.item<float>() < 0.0\n";
		q0 = q0 * -1.0;
		dot = dot * -1.0;
	}

	// sin_theta_0 can not be zero
	if (dot.item<float>() > 0.9995)
	{
		//std::cout << "dot.item<float>() > 0.9995\n";
		torch::Tensor qr = q0 + (q1 - q0) * amount;
		qr = torch::nn::functional::normalize(qr, torch::nn::functional::NormalizeFuncOptions().dim(0));
		//std::cout << "qr\n" << qr << "\n";

		return qr;
	}

	// Since dot is in range[0, 0.9995], torch::arccos() is safe
	torch::Tensor theta_0 = torch::arccos(dot);
	torch::Tensor sin_theta_0 = torch::sin(theta_0);
	torch::Tensor theta = theta_0 * amount;
	torch::Tensor sin_theta = torch::sin(theta);
	torch::Tensor s0 = torch::cos(theta) - dot * sin_theta / sin_theta_0;
	torch::Tensor s1 = sin_theta / sin_theta_0;
	torch::Tensor qr = (s0 * q0) + (s1 * q1);

	qr = torch::nn::functional::normalize(qr, torch::nn::functional::NormalizeFuncOptions().dim(0));
	//std::cout << "qr\n" << qr << "\n";

	return qr;
}

torch::Tensor
TorchQuat::slerp_debug(int wI, int bI, int sI, int jI, std::ofstream& debugOutputFile, torch::Tensor q0, torch::Tensor q1, float amount)
{
	debugOutputFile << "wI " << wI << " bI " << bI << " sI " << sI << " jI " << jI << "\n";

	assert(q0.dim() == 1 && q0.size(0) == 4);
	assert(q1.dim() == 1 && q1.size(0) == 4);

	debugOutputFile << "q0\n" << q0 << "\n";
	debugOutputFile << "q1\n" << q1 << "\n";
	debugOutputFile << "a\n" << amount << "\n";

	// Ensure quaternion inputs are unit quaternions and 0 <= amount <= 1	
	q0 = torch::nn::functional::normalize(q0, torch::nn::functional::NormalizeFuncOptions().dim(0));
	q1 = torch::nn::functional::normalize(q1, torch::nn::functional::NormalizeFuncOptions().dim(0));

	amount = std::max(std::min(amount, 1.0f), 0.0f);

	torch::Tensor dot = torch::dot(q0, q1);

	/*
	If the dot product is negative, slerp won't take the shorter path.
	Note that v1 and -v1 are equivalent when the negation is applied to all four components.
	Fix by reversing one quaternion
	*/

	if (dot.item<float>() < 0.0)
	{
		debugOutputFile << "dot.item<float>() < 0.0\n";
		q0 = q0 * -1.0;
		dot = dot * -1.0;
	}

	// sin_theta_0 can not be zero
	if (dot.item<float>() > 0.9995)
	{
		debugOutputFile << "dot.item<float>() > 0.9995\n";
		torch::Tensor qr = q0 + (q1 - q0) * amount;
		qr = torch::nn::functional::normalize(qr, torch::nn::functional::NormalizeFuncOptions().dim(0));
		debugOutputFile << "qr\n" << qr << "\n";

		return qr;
	}

	// Since dot is in range[0, 0.9995], torch::arccos() is safe
	torch::Tensor theta_0 = torch::arccos(dot);
	torch::Tensor sin_theta_0 = torch::sin(theta_0);
	torch::Tensor theta = theta_0 * amount;
	torch::Tensor sin_theta = torch::sin(theta);
	torch::Tensor s0 = torch::cos(theta) - dot * sin_theta / sin_theta_0;
	torch::Tensor s1 = sin_theta / sin_theta_0;
	torch::Tensor qr = (s0 * q0) + (s1 * q1);

	qr = torch::nn::functional::normalize(qr, torch::nn::functional::NormalizeFuncOptions().dim(0));
	debugOutputFile << "qr\n" << qr << "\n";

	return qr;
}

torch::Tensor
TorchQuat::slerp2(torch::Tensor q0, torch::Tensor q1, torch::Tensor amount)
{
	try
	{
		//std::cout << "q0\n" << q0 << "\n";
		//std::cout << "q1\n" << q1 << "\n";
		//std::cout << "a\n" << amount << "\n";

		//std::cout << "q0 s\n" << q0.sizes() << "\n";

		auto origShape = q0.sizes();

		q0 = q0.reshape({ -1, 4 });
		q1 = q1.reshape({ -1, 4 });
		amount = amount.reshape({ -1 });

		//std::cout << "amount\n" << amount << "\n";
		amount = amount.clamp(0.0, 1.0);
		//std::cout << "amount\n" << amount << "\n";
		//amount = amount.repeat({ 1, 4 });
		//std::cout << "amount\n" << amount << "\n";

		// Ensure quaternion inputs are unit quaternions and 0 <= amount <= 1	
		q0 = torch::nn::functional::normalize(q0);
		q1 = torch::nn::functional::normalize(q1);

		//std::cout << "q0 s " << q0.sizes() << "\n";
		//std::cout << "q1 s " << q1.sizes() << "\n";

		torch::Tensor dot = torch::bmm(q0.reshape({ -1, 1, 4 }), q1.reshape({ -1, 4, 1 }));
		dot = dot.squeeze();

		//std::cout << "dot s " << dot.sizes() << "\n";
		//std::cout << "dot\n" << dot << "\n";

		//for (int i = 0; i < q0.size(0); ++i)
		//{
		//	torch::Tensor dot2 = torch::dot(q0[i], q1[i]);
		//	std::cout << "i " << i << "\ndot " << dot2 << "\n";
		//}

		torch::Tensor negdot = dot > 0.0;
		//std::cout << "negdot\n" << negdot << "\n";
		negdot = negdot.to(torch::kFloat32);
		negdot = negdot * 2.0 - 1.0;
		//std::cout << "negdot\n" << negdot << "\n";

		dot = dot * negdot;
		//std::cout << "dot " << dot << "\n";

		negdot = negdot.unsqueeze(1);
		//std::cout << "negdot\n" << negdot << "\n";

		negdot = negdot.repeat({ 1, 4 });
		//std::cout << "negdot\n" << negdot << "\n";

		//std::cout << "q0\n" << q0 << "\n";
		q0 = q0 * negdot;
		//std::cout << "q0\n" << q0 << "\n";
		//std::cout << "negdot\n" << negdot << "\n";
		//std::cout << "q0\n" << q0 << "\n";

		torch::Tensor bigdot = dot > 0.9995;
		bigdot = bigdot.to(torch::kFloat32);
		//std::cout << "bigdot\n" << bigdot << "\n";

		// calculate qr as if dot was bigger than 0.9995
		torch::Tensor bigqr = q0 + (q1 - q0) * amount.reshape({ -1, 1 }).repeat({ 1, 4 });
		//std::cout << "bigqr\n" << bigqr << "\n";

		// calculate qr as if dot was smaller than 0.9995
		dot = dot.clamp_max(0.9995);
		torch::Tensor theta_0 = torch::arccos(dot);
		//std::cout << "theta_0\n" << theta_0 << "\n";

		torch::Tensor sin_theta_0 = torch::sin(theta_0);
		//std::cout << "sin_theta_0\n" << sin_theta_0 << "\n";

		torch::Tensor theta = theta_0 * amount;
		//std::cout << "theta\n" << theta << "\n";

		torch::Tensor sin_theta = torch::sin(theta);
		//std::cout << "sin_theta\n" << sin_theta << "\n";

		torch::Tensor s0 = torch::cos(theta) - dot * sin_theta / sin_theta_0;
		//std::cout << "s0\n" << s0 << "\n";
		s0 = s0.reshape({ -1, 1 }).repeat({ 1, 4 });
		//std::cout << "s0\n" << s0 << "\n";

		torch::Tensor s1 = sin_theta / sin_theta_0;
		//std::cout << "s1\n" << s1 << "\n";
		s1 = s1.reshape({ -1, 1 }).repeat({ 1, 4 });
		//std::cout << "s1\n" << s1 << "\n";

		torch::Tensor smallqr = (s0 * q0) + (s1 * q1);
		//std::cout << "smallqr\n" << smallqr << "\n";

		torch::Tensor invbigdot = torch::ones_like(bigdot) - bigdot;
		//std::cout << "invbigdot\n" << invbigdot << "\n";

		torch::Tensor qr = bigqr * bigdot.reshape({ -1, 1 }).repeat({ 1, 4 }) + smallqr * invbigdot.reshape({ -1, 1 }).repeat({ 1, 4 });
		//std::cout << "qr\n" << qr << "\n";
		qr = torch::nn::functional::normalize(qr);
		qr = qr.reshape(origShape);
		//std::cout << "qr\n" << qr << "\n";

		return qr;

		//return torch::ones_like(q1);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << "\n";
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}
}

torch::Tensor
TorchQuat::slerp2_debug(int wI, int sI, std::ofstream& debugOutputFile, torch::Tensor q0, torch::Tensor q1, torch::Tensor amount)
{
	try
	{
		debugOutputFile << "wI " << wI << " sI " << sI << "\n";

		debugOutputFile << "q0\n" << q0 << "\n";
		debugOutputFile << "q1\n" << q1 << "\n";
		debugOutputFile << "a\n" << amount << "\n";

		auto origShape = q0.sizes();

		q0 = q0.reshape({ -1, 4 });
		q1 = q1.reshape({ -1, 4 });
		amount = amount.reshape({ -1 });

		//std::cout << "amount\n" << amount << "\n";
		amount = amount.clamp(0.0, 1.0);
		//std::cout << "amount\n" << amount << "\n";
		//amount = amount.repeat({ 1, 4 });
		//std::cout << "amount\n" << amount << "\n";

		// Ensure quaternion inputs are unit quaternions and 0 <= amount <= 1	
		q0 = torch::nn::functional::normalize(q0);
		q1 = torch::nn::functional::normalize(q1);

		//std::cout << "q0 s " << q0.sizes() << "\n";
		//std::cout << "q1 s " << q1.sizes() << "\n";

		torch::Tensor dot = torch::bmm(q0.reshape({ -1, 1, 4 }), q1.reshape({ -1, 4, 1 }));
		dot = dot.squeeze();

		//std::cout << "dot s " << dot.sizes() << "\n";
		//std::cout << "dot\n" << dot << "\n";

		//for (int i = 0; i < q0.size(0); ++i)
		//{
		//	torch::Tensor dot2 = torch::dot(q0[i], q1[i]);
		//	std::cout << "i " << i << "\ndot " << dot2 << "\n";
		//}

		torch::Tensor negdot = dot > 0.0;
		//std::cout << "negdot\n" << negdot << "\n";
		negdot = negdot.to(torch::kFloat32);
		negdot = negdot * 2.0 - 1.0;
		//std::cout << "negdot\n" << negdot << "\n";

		dot = dot * negdot;
		//std::cout << "dot " << dot << "\n";

		negdot = negdot.unsqueeze(1);
		//std::cout << "negdot\n" << negdot << "\n";

		negdot = negdot.repeat({ 1, 4 });
		//std::cout << "negdot\n" << negdot << "\n";

		//std::cout << "q0\n" << q0 << "\n";
		q0 = q0 * negdot;
		//std::cout << "q0\n" << q0 << "\n";
		//std::cout << "negdot\n" << negdot << "\n";
		//std::cout << "q0\n" << q0 << "\n";

		torch::Tensor bigdot = dot > 0.9995;
		bigdot = bigdot.to(torch::kFloat32);
		//std::cout << "bigdot\n" << bigdot << "\n";

		// calculate qr as if dot was bigger than 0.9995
		torch::Tensor bigqr = q0 + (q1 - q0) * amount.reshape({ -1, 1 }).repeat({ 1, 4 });
		//std::cout << "bigqr\n" << bigqr << "\n";

		// calculate qr as if dot was smaller than 0.9995
		dot = dot.clamp_max(0.9995);
		torch::Tensor theta_0 = torch::arccos(dot);
		//std::cout << "theta_0\n" << theta_0 << "\n";

		torch::Tensor sin_theta_0 = torch::sin(theta_0);
		//std::cout << "sin_theta_0\n" << sin_theta_0 << "\n";

		torch::Tensor theta = theta_0 * amount;
		//std::cout << "theta\n" << theta << "\n";

		torch::Tensor sin_theta = torch::sin(theta);
		//std::cout << "sin_theta\n" << sin_theta << "\n";

		torch::Tensor s0 = torch::cos(theta) - dot * sin_theta / sin_theta_0;
		//std::cout << "s0\n" << s0 << "\n";
		s0 = s0.reshape({ -1, 1 }).repeat({ 1, 4 });
		//std::cout << "s0\n" << s0 << "\n";

		torch::Tensor s1 = sin_theta / sin_theta_0;
		//std::cout << "s1\n" << s1 << "\n";
		s1 = s1.reshape({ -1, 1 }).repeat({ 1, 4 });
		//std::cout << "s1\n" << s1 << "\n";

		torch::Tensor smallqr = (s0 * q0) + (s1 * q1);
		//std::cout << "smallqr\n" << smallqr << "\n";

		torch::Tensor invbigdot = torch::ones_like(bigdot) - bigdot;
		//std::cout << "invbigdot\n" << invbigdot << "\n";

		torch::Tensor qr = bigqr * bigdot.reshape({ -1, 1 }).repeat({ 1, 4 }) + smallqr * invbigdot.reshape({ -1, 1 }).repeat({ 1, 4 });
		//std::cout << "qr\n" << qr << "\n";
		qr = torch::nn::functional::normalize(qr);
		qr = qr.reshape(origShape);
		debugOutputFile << "qr\n" << qr << "\n";

		return qr;

		//return torch::ones_like(q1);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << "\n";
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}
}