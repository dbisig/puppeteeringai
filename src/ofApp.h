#pragma once

#include "ofMain.h"
#include "ofxImGui.h"
#include "dab_osc_receiver.h"
#include "dab_osc_sender.h"
#include "dab_ringbuffer.h"
#include <memory>
#include <thread>
#include <future>
#include <torch/script.h>
#include "dab_mocap_data.h"
#include "dab_vis_shape.h"
#include "dab_vis_skeleton.h"

enum skeletonJointIndices
{
	Hips,
	Spine,
	Spine1,
	Spine2,
	Spine3,
	Spine4,
	Neck,
	Head,
	Head_Nub,
	LeftShoulder,
	LeftArm,
	LeftForeArm,
	LeftHand,
	LeftHand_Nub,
	RightShoulder,
	RightArm,
	RightForeArm,
	RightHand,
	RightHand_Nub,
	LeftUpLeg,
	LeftLeg,
	LeftFoot,
	LeftToeBase,
	LeftToeBase_Nub,
	RightUpLeg,
	RightLeg,
	RightFoot,
	RightToeBase,
	RightToeBase_Nub
};

class ofApp : public ofBaseApp, public dab::OscListener
{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void mouseScrolled(int x, int y, float scrollX, float scrollY);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		void notify(std::shared_ptr<dab::OscMessage> pMessage);
		void updateOsc();
		void updateOsc(std::shared_ptr<dab::OscMessage> pMessage);
		void updateOscSender() throw (dab::Exception);

	protected:
		// mocap
		void setupMocap();
		void loadMocapFile(const std::string& pFilePath) throw (dab::Exception);
		void updateMocap();
		void updateMocap(const std::vector<float>& pMocapSample) throw (dab::Exception);
		dab::MocapDataset mMocapDataset;
		int mMocapWindowLength;
		int mMocapWindowOffset;
		int mMocapJointCount;
		int mMocapJointDim;
		int mMocapLatentDim;
		int mMocapReadFrame;
		int mMocapFrameCount;
		int mMocapStartFrame;
		int mMocapEndFrame;

		// mocap live
		dab::RingBuffer< std::array<float, 4> >* mMocapLiveRingBuffer; // mocap live
		std::vector<float> mMocapLiveValues; // mocap live
		int mMocapLiveJointMap;

		// OSC Communication
		void setupOsc() throw (dab::Exception);


		std::shared_ptr<ofApp> mSelf;
		std::mutex mOscLock;
		
		dab::OscReceiver* mOscReceiver;
		unsigned int mMaxOscMessageQueueLength = 1000;
		std::deque< std::shared_ptr<dab::OscMessage> > mOscMessageQueue;

		dab::OscSender* mOscSender;
		std::string mOscSendAddress;
		int mOscSendPort;

		void updateMocap(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception);
		void updateCamera(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception);
		void setMocapReadFrame(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception);
		void updateMocapStart(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception);
		void updateMocapEnd(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception);
		void updateEncodeIterCount(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception);
		void updateLiveIterCount(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception);
		void updateLiveJoint(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception);

		// machine learning
		void setupML(const std::string& pDiscPriorModelPath, const std::string& pEncoderModelPath, const std::string& pDecoderModelPath) throw (dab::Exception);
		void updateML() throw (dab::Exception);

		torch::jit::script::Module mDiscPrior;
		torch::jit::script::Module mEncoder;
		torch::jit::script::Module mDecoder;

		int mMocapEncodeIterCount = 10;
		int mMocapLiveIterCount = 1;

		torch::Tensor mPoseSequence;

		std::thread mMLThread;
		std::future<void> mMLThreadHandle;
		bool mMLThreadDone = true;

		// visual
		void setupGraphics() throw (dab::Exception);
		void updateGraphics() throw (dab::Exception);
		void displayGraphics() throw (dab::Exception);
		std::shared_ptr<ofCamera> mCamera;
		std::shared_ptr<ofLight> mPointLight;
		std::shared_ptr<dab::VisSkeletonPlayer> mVisSkeletonPlayer;

		std::array<float, 3> mSkeletonColor;
		float mAmbientLightIntensity;
		float mDiffuseLightIntensity;
		float mSpecularLightIntensity;

		// camera
		bool mMouseDragActive = false;
		glm::vec2 mMouseDragStartPos;
		float mCameraAzumith = 0.0;
		float mCameraElevation = 0.0;
		float mCameraDistance = 1000.0;

		bool mSkeletonPlayerIntialized = false;
		torch::Tensor mBasePose = torch::zeros({ 1 });
		torch::Tensor mPoseSequenceUpdateBuffer = torch::zeros({ 1 });
		std::vector< std::vector< glm::vec3> > mPositionSequenceUpdateVector;
		std::vector< std::vector< glm::quat> >mRotationSequenceUpdateVector;

		float mMocapFPS = 50;
		int mSeqIndex = 0;

		void updateCamera();

		// Gui
		ofxImGui::Gui mGui;
		
};
