/** \file dab_torch_quat.h
*/

#pragma once

#include <torch/script.h>
#include <assert.h>
#include "dab_singleton.h"

namespace dab
{

class TorchQuat : public Singleton<TorchQuat>
{
public:
torch::Tensor
qmul(torch::Tensor q, torch::Tensor r);

torch::Tensor
qrot(torch::Tensor q, torch::Tensor v);

torch::Tensor
qeuler(torch::Tensor q, const std::string& order, float epsilon = 0);

torch::Tensor
slerp(torch::Tensor q0, torch::Tensor q1, float amount = 0.5);

torch::Tensor
slerp2(torch::Tensor q0, torch::Tensor q1, torch::Tensor amount);

torch::Tensor
slerp_debug(int wI, int bI, int sI, int jI, std::ofstream& debugOutputFile, torch::Tensor q0, torch::Tensor q1, float amount = 0.5);

torch::Tensor
slerp2_debug(int wI, int sI, std::ofstream& debugOutputFile, torch::Tensor q0, torch::Tensor q1, torch::Tensor amount);

};

};
